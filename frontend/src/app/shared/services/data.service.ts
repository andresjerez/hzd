import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Globals } from '../../app.globals';
import { UserPreferences } from '../classes/user-preferences';
import { User } from '../classes/user';
import { Router } from '@angular/router';

declare var $: any;

@Injectable()
export class DataService {

    backendUrl: string;
    userPreferences: UserPreferences;
    user: User = null;

    constructor(private router: Router, private http: HttpClient) {
        this.backendUrl = Globals.API_BASE_URL;
    }

    /*
    Usado en Risk Matrix
     */
    saveRiskMatrix(riskDimensions: any): Observable<any> {
        const url = this.backendUrl + 'api/save_changes';
        const body = { 'risk_dimensions': riskDimensions };

        const req = this.http.post(url, body);

        return req;
    }

    changeVisibility(datos: any): Observable<any> {
        const url = this.backendUrl + 'api/change_visibility';
        const body = { 'datos': datos };

        const req = this.http.post(url, body);

        return req;
    }

    getRiskMatrixUsed() {
        const url = this.backendUrl + 'api/get_RiskMatrix_Used';
        const req = this.http.get(url);

        return req;

    }

    /*
    Usado en Risk Matrix
     */
    getRiskMatrixDimensions(): Observable<any> {
        const url = this.backendUrl + 'api/risk_matrix_dimensions';
        const req = this.http.get(url);

        return req;
    }


    getCompanyTreeStructure() {
        const url = this.backendUrl + 'dlf_api/get_tree';
        const req = this.http.get(url);

        return req;
    }

    getRiskMatrixOptions() {
        const url = this.backendUrl + 'api/fmeca_risk_matrix_options';
        const req = this.http.get(url);

        return req;
    }

    getSubsystemParts(subsystemId) {

        const url = this.backendUrl + 'api/parts?subsystem_id=' + subsystemId;
        const req = this.http.get(url);

        return req;
    }

    getMultipleSubsystemParts(subsystemsIds): Observable<any> {

        const url = this.backendUrl + 'api/parts_multiple';

        const body = { 'subsystems_ids': subsystemsIds };
        const req = this.http.post(url, body);

        return req;
    }

    saveFmecaRcm(changedNodes) {

        const url = this.backendUrl + 'api/save_fmeca_rcm_changes';

        const body = { 'fmeca_rcm_matrix': changedNodes };
        const req = this.http.post(url, body);

        return req;
    }

    /**
     * HAZOP
     * */
    saveHazop(changedNodes) {

        const url = this.backendUrl + 'api/save_hazop_changes';

        const body = { 'hazop_matrix': changedNodes };
        const req = this.http.post(url, body);

        return req;
    }

    deleteHazop(changedNodes) {

        const url = this.backendUrl + 'api/save_hazop_delete';

        const body = { 'hazop_matrix': changedNodes };
        const req = this.http.post(url, body);

        return req;
    }

    saveHazid(changedNodes) {

        const url = this.backendUrl + 'api/save_hazid_changes';

        const body = { 'hazid_matrix': changedNodes };
        const req = this.http.post(url, body);

        return req;
    }

    deleteHazid(changedNodes) {

        const url = this.backendUrl + 'api/save_hazid_delete';

        const body = { 'hazid_matrix': changedNodes };
        const req = this.http.post(url, body);

        return req;
    }

    saveHazopPartsTypes(hazopPartTypes): Observable<any> {
        const url = this.backendUrl + 'api/save_hazop_part_types';

        const body = { 'part_types': hazopPartTypes };
        const req = this.http.post(url, body);

        return req;
    }

    saveGuideWords(guideWords): Observable<any> {

        const url = this.backendUrl + 'api/save_hazop_guide_words';

        const body = { 'guide_words': guideWords };
        const req = this.http.post(url, body);

        return req;
    }

    getHazopGuideWords(): Observable<any> {

        const url = this.backendUrl + 'api/hazop_guide_words';
        const req = this.http.get(url);

        return req;
    }

    getPartTypes(): Observable<any> {

        const url = this.backendUrl + 'api/hazop_part_types';
        const req = this.http.get(url);

        return req;
    }

    getHazidCategories(): Observable<any> {

        const url = this.backendUrl + 'api/hazid_categories';
        const req = this.http.get(url);

        return req;
    }

    saveHazidCategories(hazidCategories): Observable<any> {
        const url = this.backendUrl + 'api/save_hazid_categories';

        const body = { 'hazid_categories': hazidCategories };
        const req = this.http.post(url, body);

        return req;
    }

    /**
     * Se cargan las preferencias de usuario y se almacenan para ser usadas posteriormente
     */
    loadUserPreferences() {
        this.userPreferences = new UserPreferences();
        const up = localStorage.getItem(Globals.APP_LOCAL_STORAGE_KEY);
      //  console.log(Globals.APP_LOCAL_STORAGE_KEY);
      //  console.log('up');
      //  console.log(up);
        if (up) {
            this.userPreferences = Object.assign(this.userPreferences, JSON.parse(up));
        }
    }

    getUserPreferences() {

        if (!this.userPreferences) {
            this.loadUserPreferences();
        }

        return this.userPreferences;

    }

    saveUserPreferences(preference) {
        const appLocalStorageKey = Globals.APP_LOCAL_STORAGE_KEY;
        const rmesLocalStorage = localStorage.getItem(appLocalStorageKey);

        let userPreferences = new UserPreferences();

        if (rmesLocalStorage) {
            const newPreference = $.extend(true, JSON.parse(rmesLocalStorage), preference);
            userPreferences = Object.assign(userPreferences, newPreference);
        } else {
            userPreferences = Object.assign(userPreferences, preference);
        }

        this.userPreferences = userPreferences;
        localStorage.setItem(appLocalStorageKey, JSON.stringify(userPreferences));
    }

    getDecisionTreeStructure(): Observable<any> {
        const url = this.backendUrl + 'api/decision_tree_structure';
        const req = this.http.get(url);

        return req;
    }

    saveDecisionTreeStructure(structure): Observable<any> {

        const url = this.backendUrl + 'api/save_decision_tree_structure';
        const body = { 'structure': structure };

        const req = this.http.post(url, body);

        return req;
    }

    doLogin(username, password): Observable<any> {

        const url = this.backendUrl + 'api/do_login';
        const body = { 'userdata': { 'username': username, 'password': password } };

        const req = this.http.post(url, body);

        return req;
    }

    doLogout() {
        this.destroySession();
        this.router.navigate(['/login']);
    }

    destroySession() {
        this.setUser(null);
        const url = this.backendUrl + 'api/destroy_session';
        this.http.get(url).subscribe();
    }
    

    // TODO: Mejorar sistema de validacion
    recoverSession() {

        if (this.user !== null) {
            return null;
        }

        const userStored = localStorage.getItem('rmes_user');
        if (userStored) {
            const user = JSON.parse(userStored);
            this.setUser(user); // Se pone el usuario temporalmente para que los componentes carguen editables


            const url = this.backendUrl + 'api/recover_session';
            const body = { 'token': user.token };

            this.http.post(url, body).subscribe(data => {
                if (data['status']) {
                    this.setUser(data['user']);
                } else {
                    this.doLogout();
                }
            });
        }

        return null;
    }

    setUser(user): User {
        if (user === null) {
            this.user = null;
        } else {

            this.user = new User();

            this.user.id = user.id;
            this.user.token = user.token;
            this.user.username = user.username;
            this.user.name = user.name;
            this.user.lastName = user.last_name;
            this.user.isLoggedIn = true;

            localStorage.setItem('rmes_user', JSON.stringify(this.user));
        }

        return this.user;
    }

    userCanEdit() {
        return this.user !== null && this.user.isLoggedIn;
    }
}

