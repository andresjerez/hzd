import {Injectable} from '@angular/core';
import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpErrorResponse,
    HTTP_INTERCEPTORS
} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {_throw} from 'rxjs/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/finally';
import {LoaderService} from './loader.service';

/**
 * Intercepta la respuesta HTTP, analizando y manejando errores para entregar una respuesta valida
 *
 * Esta clase actualmente intercepta TODAS las consultas HTTP de forma implicita
 */
@Injectable()
export class GeneralInterceptor implements HttpInterceptor {

    constructor(private loaderService: LoaderService) {
    }

    /**
     * Intercepts an outgoing HTTP request, executes it and handles any error that could be triggered in execution.
     * Intercepta una llamada HTTP saliente, la ejecuta y maneja cualquier error que pudiese ocurrir.
     * @see HttpInterceptor
     * @param req la consulta saliente
     * @param next HTTP request handler
     */
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.showLoader();

        console.log(req.headers);

        return next.handle(req)
            .catch(errorReponse => {
                let errMsg: string;
                if (errorReponse instanceof HttpErrorResponse) {
                    const err = errorReponse.message || JSON.stringify(errorReponse.error);
                    errMsg = `${errorReponse.status} - ${errorReponse.statusText || ''} Detalle: ${err}`;
                } else {
                    errMsg = errorReponse.message ? errorReponse.message : errorReponse.toString();
                }

                alert('Ocurrió un error inesperado en la última consulta. Por favor, intente mas tarde.\n\rSoporte:\n' + errMsg);
                return _throw(errMsg);
            }).finally(() => {
                this.hideLoader();

            });
    }

    showLoader(): void {
        this.loaderService.setLoading(true);
    }

    hideLoader(): void {
        this.loaderService.setLoading(false);
    }
}

/**
 * Provider POJO for the interceptor
 */
export const GeneralInterceptorProvider = {
    provide: HTTP_INTERCEPTORS,
    useClass: GeneralInterceptor,
    multi: true,
};
