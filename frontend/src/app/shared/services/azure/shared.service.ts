import { AuthenticationGuard } from './authentication.guard';
import { ConfigService } from './config.service';
import { AdalService } from './adal.service';
import { NgModule } from '@angular/core';

@NgModule({
  providers: [AdalService, ConfigService, AuthenticationGuard]
})
export class SharedServicesModule { }
