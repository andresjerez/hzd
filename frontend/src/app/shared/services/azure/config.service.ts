import { Injectable } from '@angular/core';

@Injectable()

export class ConfigService {

  constructor() {

  }

  public get getAdalConfig(): any {

    return {

      tenant: 'eb26686d-6e36-4da8-a5e6-a91d4c740b71',

      clientId: '58c4c817-71a9-4058-978c-34cc1e7ad7b9',

      redirectUri: window.location.origin + '/',

      postLogoutRedirectUri: window.location.origin + '/'

    };

  }

}
