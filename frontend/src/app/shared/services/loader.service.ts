import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class LoaderService {

    loading = false;
    loadingChange: Subject<boolean> = new Subject<boolean>();

    constructor() {
    }

    setLoading(status) {

       // console.log('Settting status! ' + (status ? 'true' : 'false'));
        this.loading = status;
        this.loadingChange.next(this.loading);
    }

    isLoading(): boolean {
        return this.loading;
    }

}
