export class User {

    id: number;
    username: string;
    name: string;
    lastName: string;
    role = 'guest';
    token: string;
    isLoggedIn = false;

}
