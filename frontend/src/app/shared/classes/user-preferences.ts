export class UserPreferences {

    /*
     * Estructura propuesta considera preferencias separadas por módulo de la suite
     * {
     *      keyModulo1: { keyPersonalizada1: valor }
     *      keyModulo2: { keyPersonalizada1: valor }
     * }
     */
    rkm: any = {};
    fmeca: any = {};
    rcm: any = {};

}
