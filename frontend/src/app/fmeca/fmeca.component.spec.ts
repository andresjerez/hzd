import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FmecaComponent } from './fmeca.component';

describe('FmecaComponent', () => {
  let component: FmecaComponent;
  let fixture: ComponentFixture<FmecaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FmecaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FmecaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
