import { HazidCategoriesComponent } from './hazid/hazid-categories/hazid-categories.component';
import {
  Component, OnInit, ViewChild,
  ViewContainerRef,
  ComponentFactoryResolver
} from '@angular/core';
import { RiskMatrixComponent } from '../risk-matrix/risk-matrix.component';
import { PartsComponent } from './parts/parts.component';
import { GuideWordsComponent } from './hazop/guide-words/guide-words.component';

declare var $: any;
declare var UIkit: any;
declare var Highcharts: any;

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css'],
  entryComponents: [RiskMatrixComponent, PartsComponent, GuideWordsComponent, HazidCategoriesComponent]
})

export class SettingsComponent implements OnInit {
  moduleInfo: any = { title: 'RMES Settings', key: 'rmes-settings' };
  configSelected = false;

  configLocations = {
    'risk-matrix': RiskMatrixComponent,
    'hazop-parts': PartsComponent,
    'hazop-guide-words': GuideWordsComponent,
    'hazid-categories': HazidCategoriesComponent
  };

  @ViewChild('componentHost', { read: ViewContainerRef }) entry: ViewContainerRef;

  constructor(private resolver: ComponentFactoryResolver) { }

  ngOnInit() {
  }

  accessConfig(configRoute) {

    if (!configRoute || !this.configLocations[configRoute]) { return; }

    this.configSelected = true;
    this.entry.clear();

    const factory = this.resolver.resolveComponentFactory(this.configLocations[configRoute]);
    const componentRef = this.entry.createComponent(factory);

    componentRef.changeDetectorRef.detectChanges();
  }
}
