import { DataService } from './../../../shared/services/data.service';
import { Component, OnInit } from '@angular/core';

declare var UIkit: any;

@Component({
  selector: 'app-hazid-categories',
  templateUrl: './hazid-categories.component.html',
  styleUrls: ['./hazid-categories.component.css']
})
export class HazidCategoriesComponent implements OnInit {

  categories: any = null;
  currentCategory = null;
  statSend = false;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.loadCategories();
  }

  loadCategories() {
    this.dataService.getHazidCategories().subscribe((data) => {
      if (data.status) {
        this.categories = data.hazid_categories;
      }
    });
  }

  selectCategory(category) {
    this.currentCategory = category;
  }

  addCategory() {

    UIkit.modal.prompt('Nombre de Nueva Categoría:', '').then((categoryName) => {

      categoryName = categoryName.trim();
      if (!categoryName || categoryName.length === 0) {
        return;
      }

      const newCategory = {
        id: null,
        name: categoryName,
        status: 1,
        guide_words: []
      };

      this.categories.push(newCategory);
      this.currentCategory = this.categories[this.categories.length - 1];
    });


  }

  removeCategory(category) {

    UIkit.modal.confirm('Está seguro que desea eliminar esta categoría? Todos sus palabras guías serán eliminadas!').then(() => {
      const categoryIndex = this.categories.indexOf(category);

      if (categoryIndex !== -1) {
        const dCategory = this.categories[categoryIndex];
        dCategory.status = 0;

        // Se marcan sus palabras guias para borrado
        dCategory.guide_words.forEach(g => {
          g.status = 0;
        });
      }
      this.saveChanges();
    });
  }

  addGuideWord() {

    UIkit.modal.prompt('Palabra Guía:', '').then((guideWord) => {
      guideWord = guideWord.trim();
      if (!guideWord || guideWord.length === 0) {
        return;
      }

      const newGuideWord = {
        id: null,
        guide_word: guideWord,
        status: 1
      };

      this.currentCategory.guide_words.push(newGuideWord);
    });
  }

  removeGuideWord(guideWord) {

    UIkit.modal.confirm('Está seguro que desea eliminar esta palabra guía?').then(() => {
      const guideWordIndex = this.currentCategory.guide_words.indexOf(guideWord);

      if (guideWordIndex !== -1) {
        const dGuideWord = this.currentCategory.guide_words[guideWordIndex];
        dGuideWord.status = 0;
        this.saveChanges();
      }
    }, function () {
      console.log('Rejected.');
    });

  }

  updateCategoryName(targetObj, updateLabel) {

    UIkit.modal.prompt(updateLabel + ':', targetObj.name).then(function (name) {
      if (name && name.trim().length > 0) {
        targetObj.name = name;
      }
    });
  }

  updateGuideWordName(targetObj, updateLabel) {

    UIkit.modal.prompt(updateLabel + ':', targetObj.guide_word).then(function (name) {
      if (name && name.trim().length > 0) {
        targetObj.guide_word = name;
      }
    });
  }

  saveChanges() {
    this.dataService.saveHazidCategories(this.categories).subscribe((data) => {
      if (data.status) {
        this.categories = null;
        this.loadCategories();
        this.statSend = false;
        UIkit.notification({
          message: 'Los cambios han sido guardados',
          status: 'success',
          pos: 'top-center',
          timeout: 4000
        });
      }
    }, (error) => {
      UIkit.notification({
        message: 'Ocurrió un error al guardar los cambios!',
        status: 'danger',
        pos: 'top-center',
        timeout: 4000
      });
    });
  }

  checkSubmit() {
    if (this.statSend == false) {
        this.statSend = true; 
        this.saveChanges();
    }
  }

}
