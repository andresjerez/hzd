import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HazidCategoriesComponent } from './hazid-categories.component';

describe('HazidCategoriesComponent', () => {
  let component: HazidCategoriesComponent;
  let fixture: ComponentFixture<HazidCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HazidCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HazidCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
