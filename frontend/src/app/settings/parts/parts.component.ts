import { DataService } from './../../shared/services/data.service';
import { Component, OnInit } from '@angular/core';

declare var UIkit: any;

@Component({
  selector: 'app-parts',
  templateUrl: './parts.component.html',
  styleUrls: ['./parts.component.css']
})

export class PartsComponent implements OnInit {

  partTypes: any = null;
  currentPartType = null;
  statSend = false;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.loadPartTypes();
  }

  loadPartTypes() {
    this.dataService.getPartTypes().subscribe((data) => {
      if (data.status) {
        this.partTypes = data.part_types;
      }
    });
  }

  selectPartType(partType) {
    this.currentPartType = partType;
  }

  addType() {

    UIkit.modal.prompt('Nombre de Nuevo Tipo:', '').then((partTypeName) => {

      partTypeName = partTypeName.trim();
      if (!partTypeName || partTypeName.length === 0) {
        return;
      }

      const newPartType = {
        id: null,
        name: partTypeName,
        status: 1,
        parameters: [],
        actions: []
      };

      this.partTypes.push(newPartType);
      this.currentPartType = this.partTypes[this.partTypes.length - 1];
    });


  }


  duplicateType(partType) {
    const newPartType = JSON.parse(JSON.stringify(partType));

    newPartType.name = newPartType.name + ' Copia';
    newPartType.status = parseInt(newPartType.status, 10);

    // Se borran las ids para su insercion en backend
    newPartType.id = null;
    newPartType.actions.forEach(a => {
      a.id = null;
      a.status = parseInt(a.status, 10);
    });
    newPartType.parameters.forEach(p => {
      p.id = null;
      p.status = parseInt(p.status, 10);
    });

    this.partTypes.push(newPartType);
  }

  removeType(partType) {

    UIkit.modal.confirm('Está seguro que desea eliminar este tipo? Todos sus parámetros y acciones serán eliminadas!').then(() => {
      const partIndex = this.partTypes.indexOf(partType);

      if (partIndex !== -1) {
        const dPartType = this.partTypes[partIndex];
        dPartType.status = 0;

        // Se marcan sus acciones y parametros para borrado
        dPartType.actions.forEach(a => {
          a.status = 0;
        });
        dPartType.parameters.forEach(p => {
          p.status = 0;
        });
        this.saveChanges();
      }
    });
  }

  addParameter() {

    UIkit.modal.prompt('Nombre de Nuevo Parámetro:', '').then((paramName) => {
      paramName = paramName.trim();
      if (!paramName || paramName.length === 0) {
        return;
      }

      const newParam = {
        id: null,
        name: paramName,
        status: 1
      };

      this.currentPartType.parameters.push(newParam);
    });
  }

  removeParameter(parameter) {

    UIkit.modal.confirm('Está seguro que desea eliminar este parámetro?').then(() => {
      const paramIndex = this.currentPartType.parameters.indexOf(parameter);

      if (paramIndex !== -1) {
        const dParam = this.currentPartType.parameters[paramIndex];
        dParam.status = 0;
      }
      this.saveChanges();
    });

  }

  addAction() {

    UIkit.modal.prompt('Nombre de Nueva Acción:', '').then((actionName) => {
      actionName = actionName.trim();
      if (!actionName || actionName.length === 0) {
        return;
      }

      const newAction = {
        id: null,
        name: actionName,
        status: 1
      };

      this.currentPartType.actions.push(newAction);
    });
  }

  removeAction(action) {

    UIkit.modal.confirm('Está seguro que desea eliminar esta acción?').then(() => {
      const actionIndex = this.currentPartType.actions.indexOf(action);

      if (actionIndex !== -1) {
        const dParam = this.currentPartType.actions[actionIndex];
        dParam.status = 0;
      }

      this.saveChanges();
    }, function () {
      console.log('Rejected.');
    });

  }

  updateName(targetObj, updateLabel) {

    UIkit.modal.prompt(updateLabel + ':', targetObj.name).then(function (name) {
      if (name && name.trim().length > 0) {
        targetObj.name = name;
      }
    });
  }

  saveChanges() {
    this.dataService.saveHazopPartsTypes(this.partTypes).subscribe((data) => {
      if (data.status) {
        this.partTypes = null;
        this.loadPartTypes();
        this.statSend = false;
        UIkit.notification({
          message: 'Los cambios han sido guardados',
          status: 'success',
          pos: 'top-center',
          timeout: 4000
        });
      }
    }, (error) => {
      UIkit.notification({
        message: 'Ocurrió un error al guardar los cambios!',
        status: 'danger',
        pos: 'top-center',
        timeout: 4000
      });
    });
  }

  checkSubmit() {
    if (this.statSend == false) {
        this.statSend = true; 
        this.saveChanges();
    }
  }

}
