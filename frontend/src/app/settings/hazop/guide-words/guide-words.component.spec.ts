import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuideWordsComponent } from './guide-words.component';

describe('GuideWordsComponent', () => {
  let component: GuideWordsComponent;
  let fixture: ComponentFixture<GuideWordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuideWordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuideWordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
