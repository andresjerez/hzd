import { DataService } from './../../../shared/services/data.service';
import { Component, OnInit } from '@angular/core';

declare var UIkit: any;

@Component({
  selector: 'app-guide-words',
  templateUrl: './guide-words.component.html',
  styleUrls: ['./guide-words.component.css']
})

export class GuideWordsComponent implements OnInit {

  guideWords: any = null;
  statSend = false; 
  constructor(private dataService: DataService) { }

  ngOnInit() {

    this.dataService.getHazopGuideWords().subscribe((data) => {

      if (data.status) {
        this.guideWords = data.guide_words;
      }
    });
  }

  addGuideWord() {
    const guideWord = { id: null, guide_word: '', description: '', status: 1 };
    this.guideWords.push(guideWord);
  }

  deleteGuideWord(row) {

    const parent = this;
    UIkit.modal.confirm('Está seguro que desea eliminar la fila seleccionada?').then(function () {

      row.status = 0;

      UIkit.notification({
        message: 'La fila ha sido eliminada.',
        status: 'success',
        pos: 'top-center',
        timeout: 4000
      });

      parent.saveChanges();
    }, function () {
      console.log('Delete rejected.');
    });

  }

  saveChanges() {
    this.dataService.saveGuideWords(this.guideWords).subscribe(
      (data) => {
        if (data.status) {
          this.statSend = false; 
          UIkit.notification({
            message: 'Se han guardado los cambios.',
            status: 'success',
            pos: 'top-center',
            timeout: 4000
          });
        }
      }
    );
  }

  checkSubmit() {
    if (this.statSend == false) {
        this.statSend = true; 
        this.saveChanges();
    }
  }

}
