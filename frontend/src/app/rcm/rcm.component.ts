import {AfterViewInit, Component, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import * as XLSX from 'xlsx';
import {saveAs} from 'file-saver';
import {DataService} from '../shared/services/data.service';
import { DxDataGridComponent } from 'devextreme-angular';

declare const $: any;
declare const UIkit: any;
declare const Highcharts: any;


@Component({
    selector: 'app-rcm',
    templateUrl: './rcm.component.html',
    styleUrls: ['./rcm.component.css'],
    encapsulation: ViewEncapsulation.None
})

export class RcmComponent implements AfterViewInit, OnInit {

    moduleInfo: any = {'title': 'RCM', 'key': 'rcm'};

    @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;
    // Variables para las funciones de filtro y búsqueda del datagrid
    applyFilterTypes: any;
    currentFilter: any;
    showFilterRow: boolean;
    showHeaderFilter: boolean;

    isNaN = (isNaN); // Se expone la funcion isNaN para su uso en template
    moduleFields: any = [
        {
            'id': 0,
            'key': 'id',
            'name': 'Código',
            'type': 'readonly',
            'visible': false,
            'parent_module': 'fmeca',
            'required': false,
            'children': []
        },
        {
            'id': 0,
            'key': 'function',
            'name': 'Función',
            'visible': true,
            'parent_module': 'fmeca',
            'required': false,
            'children': []
        },
        {
            'id': 0,
            'key': 'functional-failure',
            'name': 'Falla funcional',
            'visible': true,
            'parent_module': 'fmeca',
            'required': false,
            'children': []
        },
        {
            'id': 0,
            'key': 'failure-mode',
            'name': 'Modo de falla',
            'visible': true,
            'parent_module': 'fmeca',
            'required': true,
            'children': []
        },
        {
            'id': 0,
            'key': 'maintenable-element',
            'name': 'Elemento mantenible',
            'visible': true,
            'parent_module': 'fmeca',
            'required': true,
            'children': []
        },
        {
            'id': 0,
            'key': 'cause',
            'name': 'Causa',
            'visible': true,
            'parent_module': 'fmeca',
            'required': false,
            'children': []
        },
        {
            'id': 0,
            'key': 'effect',
            'name': 'Efecto',
            'visible': true,
            'parent_module': 'fmeca',
            'children': [
                {
                    'id': 0,
                    'key': 'effect-inmediate',
                    'name': 'Inmediato',
                    'visible': true,
                    'parent_module': 'fmeca',
                    'required': true
                },
                {
                    'id': 0,
                    'key': 'effect-asset',
                    'name': 'Equipo',
                    'visible': true,
                    'parent_module': 'fmeca',
                    'required': true
                },
                {
                    'id': 0,
                    'key': 'effect-process',
                    'name': 'Proceso',
                    'visible': true,
                    'parent_module': 'fmeca',
                    'required': true
                }
            ]
        },
        {
            'id': 0,
            'key': 'symptom',
            'name': 'Síntoma',
            'visible': true,
            'parent_module': 'fmeca',
            'required': false,
            'children': []
        },
        {
            'id': 0,
            'key': 'detection-method',
            'name': 'Método de detección',
            'visible': true, 'parent_module': 'fmeca', 'required': true,
            'type': 'select',
            'children': [],
            'options': 'risk-matrix_detection-detection'
        },
        {
            'id': 0,
            'key': 'evident',
            'name': 'Evidente', 'visible': true, 'parent_module': 'fmeca', 'required': true,
            'type': 'select-simple',
            'options': [
                {'value': false, 'name': 'No'},
                {'value': true, 'name': 'Si'}
            ],
            'children': []
        },
        {
            'id': 0,
            'key': 'frequency',
            'name': 'Frecuencia', 'visible': true, 'parent_module': 'fmeca', 'required': true,
            'children': [],
            'type': 'select',
            'options': 'risk-matrix_frequency-frequency'
        },
        {
            'id': 0,
            'key': 'consequence',
            'name': 'Consecuencia',
            'type': 'select', 'visible': true, 'parent_module': 'fmeca', 'required': true,
            'options': 'risk-matrix_consequence',
            'children': [
                {
                    'id': 0,
                    'key': 'consequence-security',
                    'name': 'Seguridad',
                    'type': 'select', 'visible': true, 'parent_module': 'fmeca', 'required': true,
                    'options': 'risk-matrix_consequence-security'
                },
                {
                    'id': 0,
                    'key': 'consequence-environment',
                    'name': 'Medio ambiente',
                    'type': 'select', 'visible': true, 'parent_module': 'fmeca', 'required': true,
                    'options': 'risk-matrix_consequence-environment'
                },
                {
                    'id': 0,
                    'key': 'consequence-community',
                    'name': 'Comunidad',
                    'type': 'select', 'visible': true, 'parent_module': 'fmeca', 'required': true,
                    'options': 'risk-matrix_consequence-community'
                },
                {
                    'id': 0,
                    'key': 'consequence-reputation',
                    'name': 'Reputación',
                    'type': 'select', 'visible': true, 'parent_module': 'fmeca', 'required': true,
                    'options': 'risk-matrix_consequence-reputation'
                },
                {
                    'id': 0,
                    'key': 'consequence-legal',
                    'name': 'Legal',
                    'type': 'select', 'visible': true, 'parent_module': 'fmeca', 'required': true,
                    'options': 'risk-matrix_consequence-legal'
                },
                {
                    'id': 0,
                    'key': 'consequence-finance',
                    'name': 'Financiera',
                    'type': 'select', 'visible': true, 'parent_module': 'fmeca', 'required': true,
                    'options': 'risk-matrix_consequence-finance'
                }
            ]
        },
        {
            'id': 0,
            'key': 'risk',
            'name': 'Riesgo',
            'type': 'readonly',
            'visible': true,
            'parent_module': 'fmeca',
            'required': true,
            'children': []
        },
        {
            'id': 0,
            'key': 'strategy',
            'name': 'Estrategia',
            'type': 'select-simple',
            'visible': true, 'parent_module': 'rcm', 'required': true,
            'children': [],
            'options': [{'value': 'preventive', 'name': 'Preventivo'},
                {'value': 'condition', 'name': 'Condición'},
                {'value': 'corrective', 'name': 'Correctivo'},
                {'value': 'redesign', 'name': 'Rediseño'},
                {'value': 'inspection-hidden-failures', 'name': 'Insp. fallas ocultas'},
                {'value': 'preventive-condition', 'name': 'Preventivo & Condición'},
                {'value': 'inspection-hidden-failures-preventive', 'name': 'Insp. fallas ocultas & Preventivo'},
                {'value': 'inspection-hidden-failures-condition', 'name': 'Insp. fallas ocultas & Condición'}]
        },
        {
            'id': 0,
            'key': 'control-method',
            'name': 'Método de control',
            'type': 'text',
            'visible': true, 'parent_module': 'rcm', 'required': true,
            'children': []
        },
        {
            'id': 0,
            'key': 'control-frequency',
            'name': 'Frecuencia de control',
            'type': 'number',
            'visible': true, 'parent_module': 'rcm', 'required': true,
            'children': []
        },
        {
            'id': 0,
            'key': 'frequency-unit',
            'name': 'Unidad de frecuencia',
            'type': 'select-simple',
            'visible': true, 'parent_module': 'rcm', 'required': true,
            'children': [],
            'options': [
                {'value': 'hour', 'name': 'Horas'},
                {'value': 'day', 'name': 'Días'},
                {'value': 'month', 'name': 'Meses'},
                {'value': 'year', 'name': 'Años'},
                {'value': 'hours-op', 'name': 'Horas op.'},
                {'value': 'cycle', 'name': 'Ciclos'},
                {'value': 'tkph', 'name': 'tkph'}
            ]
        },


        // maintenance plan
        {
            'id': 0,
            'key': 'activity-plan',
            'name': 'Actividad plan',
            'type': 'select-simple',
            'visible': false,
            'parent_module': 'maintenance-plan', 'required': false,
            'options': [
                {'value': 'pm', 'name': 'PM'},
                {'value': 'inspection', 'name': 'Inspección'},
                {'value': 'part-change', 'name': 'Cambio de parte'},
                {'value': 'adjustment', 'name': 'Ajuste'}
            ],
            'children': []
        },
        {
            'id': 0,
            'key': 'description',
            'name': 'Descripción',
            'type': 'text',
            'visible': false,
            'parent_module': 'maintenance-plan', 'required': false,
            'children': []
        },
        {
            'id': 0,
            'key': 'specialty',
            'name': 'Especialidad',
            'type': 'select-simple',
            'visible': false,
            'parent_module': 'maintenance-plan', 'required': false,
            'options': [
                {'value': 'mechanical', 'name': 'Mecánico'},
                {'value': 'electrical', 'name': 'Eléctrico'},
                {'value': 'instrumentation', 'name': 'Instrumentación'},
                {'value': 'control', 'name': 'Control'},
                {'value': 'lubrication', 'name': 'Lubricación'},
                {'value': 'vulcanization', 'name': 'Vulcanización'}
            ],
            'children': []
        },
        {
            'id': 0,
            'key': 'plan-frequency',
            'name': 'Frecuencia plan',
            'type': 'number',
            'visible': false,
            'parent_module': 'maintenance-plan', 'required': false,
            'children': []
        },
        {
            'id': 0,
            'key': 'plan-frequency-unit',
            'name': 'Unidad frecuencia plan',
            'type': 'select-simple',
            'visible': false,
            'parent_module': 'maintenance-plan', 'required': false,
            'options': [
                {'value': 'hour', 'name': 'Horas'},
                {'value': 'day', 'name': 'Días'},
                {'value': 'month', 'name': 'Meses'},
                {'value': 'year', 'name': 'Años'},
                {'value': 'hours-op', 'name': 'Horas op.'},
                {'value': 'cycle', 'name': 'Ciclos'},
                {'value': 'tkph', 'name': 'tkph'}
            ],
            'children': []
        },
        {
            'id': 0,
            'key': 'hh-plan',
            'name': 'HH plan',
            'type': 'number',
            'visible': false,
            'parent_module': 'maintenance-plan', 'required': false,
            'children': []
        },
        {
            'id': 0,
            'key': 'staff',
            'name': 'Dotación',
            'type': 'number',
            'visible': false,
            'parent_module': 'maintenance-plan', 'required': false,
            'children': []
        },
        {
            'id': 0,
            'key': 'duration',
            'name': 'Duración',
            'type': 'number',
            'visible': false,
            'parent_module': 'maintenance-plan', 'required': false,
            'children': []
        },
        {
            'id': 0,
            'key': 'parts',
            'name': 'Repuestos',
            'type': 'text',
            'visible': false,
            'parent_module': 'maintenance-plan', 'required': false,
            'children': []
        },
        {
            'id': 0,
            'key': 'tools',
            'name': 'Herramientas',
            'type': 'text',
            'visible': false,
            'parent_module': 'maintenance-plan', 'required': false,
            'children': []
        },
        {
            'id': 0,
            'key': 'supplies',
            'name': 'Insumos',
            'type': 'text',
            'visible': false,
            'parent_module': 'maintenance-plan', 'required': false,
            'children': []
        },
        {
            'id': 0,
            'key': 'support-assets',
            'name': 'Equipos de apoyo',
            'type': 'text',
            'visible': false,
            'parent_module': 'maintenance-plan', 'required': false,
            'children': []
        }
    ];

    riskMatrix: any = {};

    isNewFormVisible = false;

    tableSelects: any = {
        frequency_frequency: null,
        consequence_environment: null,
        consequence_finance: null,
        consequence_legal: null,
        consequence_reputation: null,
        consequence_security: null,
        consequence_community: null,
        detection_detection: null,
        confirmation: null,
        strategy: null,
        frequency_unit: null
  
    };

    currentNode: any = null;
    currentPart: any = null;
    changedNodes: any = [];
    tempNodeId = 0;

    newPart: any = null;
    title = 'RCM';
    treeContainerId = '#cgs-dlf';

    oc = null;
    ocTreeVisible = false;
    ocTreeMaximized = false;
    ocEditTools = true;

    datasource: any;
    editEnabled: boolean;

    constructor(private dataService: DataService) {
        this.editEnabled = this.dataService.userCanEdit();
        this.showFilterRow = true;
        this.showHeaderFilter = true;
        this.applyFilterTypes = [{
            key: "auto",
            name: "Immediately"
        }, {
            key: "onClick",
            name: "On Button Click"
        }];
    }


    ngOnInit() {

        this.applyUserPreferences();

        this.dataService.getRiskMatrixOptions()
            .subscribe(data => {
                this.riskMatrix = data['risk_matrix_options'];
                this.fillSelects(this.riskMatrix);
            });


        this.dataService.getDecisionTreeStructure()
            .subscribe(data => {
                this.datasource = data['structure'];
                this.drawDecisionTree();
            });
    }

    logEvent(object){
        var k= this.deleteFailureMode(object);
        if (k)this.dataGrid.instance.deleteRow(object.rowIndex);  
    }
    deleteRow(object){
        object.data.status = 0;
        this.saveChanges(); 
    }


    fillSelects(data){
       
        this.tableSelects.frequency_frequency = data['risk-matrix_frequency-frequency'].data;
        this.tableSelects.consequence_community = data['risk-matrix_consequence-community'].data; 
        this.tableSelects.consequence_environment = data['risk-matrix_consequence-environment'].data;
        this.tableSelects.consequence_finance = data['risk-matrix_consequence-finance'].data;
        this.tableSelects.consequence_legal = data['risk-matrix_consequence-legal'].data;
        this.tableSelects.consequence_reputation = data['risk-matrix_consequence-reputation'].data;
        this.tableSelects.consequence_security = data['risk-matrix_consequence-security'].data;
        this.tableSelects.detection_detection = data['risk-matrix_detection-detection'].data;      
        this.tableSelects.confirmation = [{description: "No", value:"false"}, {description: "Sí", value: "true"}];
        this.tableSelects.strategy = this.getSelectOptions('strategy');
        this.tableSelects.frequency_unit = this.getSelectOptions('frequency-unit');
        

    }

    getSelectOptions(option){
        for (let i = 0; i < this.moduleFields.length; i++) {
            const row = this.moduleFields[i];
            if (row.key == option){
                return row.options;
            }
        }
    }

    onSelectionChanged(e, dropDownBoxInstance){
        var keys = e.selectedRowKeys;
      
        dropDownBoxInstance.option("value", keys.length> 0 ? keys[0] : null);
     }
     onValueChanged(args, setValueMethod){
        
         setValueMethod(args.value);
     }

    initColumns(){

        for (let i = 0; i < this.moduleFields.length; i++) {
            const field = this.moduleFields[i];
            this.hideColumn(field.id);
        }
        
    }

    hideColumn(columnName) {
        this.dataGrid.instance.columnOption(columnName, "visible", false);
    }

    setCellValue (newData, value, currentRowData) {
        console.log("Ok");

              
    }

    updateRisk() {
        for (let i = 0; i < this.currentPart.fmeca.data.length; i++) {
            const row = this.currentPart.fmeca.data[i];
            this.calculateRowRisk(row);
        }

    }

    ngAfterViewInit() {
        $.datetimepicker.setLocale('es');
        this.getCompanyTree();
    }

    /**
     * Se obtienen los nodos del DLF y luego se genera el arbol
     */
    getCompanyTree() {
        this.dataService.getCompanyTreeStructure()
            .subscribe(data => this.generateCompanyTree(data, this.treeContainerId));
    }

    /**
     * Se genera el arbol DLF y se configuran sus eventos
     * @param tree arbol de nodos del DLF
     * @param selector selector DOM donde se dibujara el arbol
     */
    generateCompanyTree(tree, selector) {

        const IMG_URL = './assets/img';
        $(selector).jstree('destroy');
        $.jstree.defaults.checkbox.three_state = false;
        const parent = this;
        const treeTypes = {
            default: {
                icon: IMG_URL + '/jstree_icons/Search.png'
            },
            graph: {
                icon: IMG_URL + '/jstree_icons/corporation.png'
            },
            area: {
                icon: IMG_URL + '/jstree_icons/equipPlant.png'
            },
            linearmachine: {
                icon: IMG_URL + '/jstree_icons/line.png'
            },
            equipment: {
                icon: IMG_URL + '/jstree_icons/equip.png'
            },
            standbymachine: {
                icon: IMG_URL + '/jstree_icons/standby.png'
            },
            fractionmachine: {
                icon: IMG_URL + '/jstree_icons/fraction.png'
            },
            redundantmachine: {
                icon: IMG_URL + '/jstree_icons/redundant.png'
            },
            parallelmachine: {
                icon: IMG_URL + '/jstree_icons/parallel.png'
            },
            stockpile: {
                icon: IMG_URL + '/jstree_icons/stockpile.png'
            }
        };
        // Crear una instancia de js tree
        //noinspection TypeScriptUnresolvedFunction
        $(selector).jstree({
            'core': {
                data: tree,
                check_callback: true
            },
            types: treeTypes,
            state: {key: 'rmes-suite'},
            plugins: ['search', 'types', 'state']
        });

        const companyTree = $(selector).jstree(true);
        let to = null;
        $('#tree-search').keyup(function () {
            if (to) {
                clearTimeout(to);
            }
            to = setTimeout(function () {
                const v = $('#tree-search').val();
                if (v.length >= 3 || v.length === 0) {
                    $(selector).jstree(true).search(v);
                }
            }, 250);
        });
        // Listener para click en elementos de jstree
        $(selector).on('changed.jstree', function (e, data) {
            const nodes = companyTree.get_selected(true);


            if (nodes.length > 0 && nodes[0].data.isleaf === '1') { // TODO: Clean json input
                parent.currentNode = nodes[0];
                parent.configureNode();
            }

        });

        // Listener para reseleccionar nodo tras un reload
        $(selector).on('ready.jstree', function (e, data) {
            if (parent.tempNodeId !== 0) {

                const nodes = $(selector).jstree(true).get_json('#', {flat: true});

                for (let i = 0; i < nodes.length; i++) {
                    const n = nodes[i];

                    if (n.data.subsystem_ref === parent.tempNodeId) {

                        $(selector).jstree().select_node(n.id);
                        parent.tempNodeId = 0;
                        break;
                    }
                }
            }
        });
    }

    configureNode() {

        this.currentPart = null;
        if (typeof this.currentNode.parts === 'undefined') {
            this.getSubsystemParts(this.currentNode);
        }
        const parent = $(this.treeContainerId).jstree().get_node(this.currentNode.parent);

        const nodeIndex = this.changedNodes.indexOf(this.currentNode);
        if (nodeIndex === -1) {
            this.changedNodes.push(this.currentNode);
        } else {
            this.changedNodes[nodeIndex] = this.currentNode;
        }

        this.currentNode.parent = parent;
    }

    getSubsystemParts(node) {

        const subsystemId = node.data.subsystem_ref;

        if (subsystemId) {
            const treeData = this.dataService.getSubsystemParts(subsystemId)
                .subscribe(data => {
                    node.parts = data['parts'];
                    this.validateMatrix();
                });
        }
    }

    onSelectPart(part) {
        this.currentPart = part;
    }

    createPart() {

        this.newPart = {
            'id': null,
            'subsystem_id': this.currentNode.data.subsystem_ref,
            'name': 'Parte #' + (this.currentNode.parts.length + 1),
            'fmeca': {
                'fmeca_status': false,
                'rcm_status': false,
                'status': 1,
                'data': []
            },
            'version': '0.1',
            'date_modified': '',
            'status': 1
        };

        this.setupDatepickerFields();
        UIkit.modal('#part-new-modal').show();

    }

    addPart() {

        this.currentNode.parts.push(this.newPart);

        // Focus on the new part
        $('#cgs-fmeca-navigator').animate({scrollTop: $('#cgs-fmeca-navigator table').height()}, 100);
        this.onSelectPart(this.currentNode.parts[this.currentNode.parts.length - 1]);

        UIkit.notification({
            message: 'La parte ha sido agregada',
            status: 'success',
            pos: 'top-center',
            timeout: 4000
        });

        UIkit.modal('#part-new-modal').hide();

    }

    editPart(part) {
        this.setupDatepickerFields();
        UIkit.modal('#part-edit-modal').show();
    }

    savePart() {
        
        UIkit.notification({
            message: 'La parte ha sido actualizada',
            status: 'success',
            pos: 'top-center',
            timeout: 4000
        });
        UIkit.modal('#part-edit-modal').hide();
    }

    deletePart(part) {
        const parent = this;
        UIkit.modal.confirm('Está seguro que desea borrar la parte <b>' + part.name + '</b>').then(function () {
            part.status = 0;
            parent.currentPart = null;
            UIkit.notification({
                message: 'La parte ha sido borrada',
                status: 'success',
                pos: 'top-center',
                timeout: 4000
            });
        }, function () {
            console.log('Rejected.');
        });
        part.status = 0;
        this.saveChanges();
    }

    saveChanges() {

        this.validateMatrix();
        this.dataService.saveFmecaRcm(this.changedNodes).subscribe(() => {

            // Se guarda el nodo actual para reabrirlo luego de recargar
            this.tempNodeId = this.currentNode.data.subsystem_ref;

            // Recargar datos para mantener consistencia
            this.currentNode = null;
            this.currentPart = null;
            this.changedNodes = [];
            this.getCompanyTree();

            UIkit.notification({
                message: 'Cambios guardados exitósamente',
                status: 'success',
                pos: 'top-center',
                timeout: 4000
            });
        });
    }

    graphNpr() {

        if (!this.currentPart || this.currentPart.fmeca.data.length === 0) {

            UIkit.notification({
                message: 'Aún no hay elementos en FMECA',
                status: 'warning',
                pos: 'top-center',
                timeout: 4000
            });
            return;
        }


        const serie = {name: 'Riesgo (NPR)', data: []};
        const categories = [];
        let auxArray = JSON.parse(JSON.stringify(this.currentPart.fmeca.data)); // Copiar arreglo para operar con ellos

        // Poner un nombre a elementos anonimos
        for (let i = 0; i < auxArray.length; i++) {
            if (!auxArray[i]['failure-mode']) {
                auxArray[i]['failure-mode'] = 'Sin Nombre ' + (i + 1);
            }
        }

        // Se ordena de mayor a menor
        auxArray = auxArray.sort(function (a, b) {

            if (isNaN(a.risk)) {
                return 1;
            }
            if (isNaN(b.risk)) {
                return -1;
            }

            return b.risk - a.risk;
        });

        // Se crean las variables para Highcharts
        for (let i = 0; i < auxArray.length; i++) {
            const row = auxArray[i];

            categories.push(row['failure-mode']);
            serie.data.push(isNaN(row.risk) ? null : parseInt(row.risk, 10));
        }

        const parent = this;
        UIkit.modal('#risk-chart-modal').show();
        Highcharts.chart('risk-chart-container', {
            chart: {
                type: 'column'
            },
            title: {
                text: parent.currentPart.name
            },
            subtitle: {
                text: 'Distribución del Riesgo'
            },
            xAxis: {
                categories: categories,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Riesgo (NPR)'
                }
            },
            credits: {
                enabled: false
            },
            legend: {
                enabled: false
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [serie]
        });
    }

   testie(rowIndex){
    this.dataGrid.instance.deleteRow(rowIndex);  
   }

    addFailureMode() {
        this.currentPart.fmeca.data.push({'class': 'high', 'id': null, status: 1});
        this.dataGrid.instance.refresh();
    }

    deleteFailureMode(row) {
        var k = false;
        const parent = this
        UIkit.modal.confirm('Está seguro que desea borrar esta fila?').then(function () {
            row.data.status = 0;               
            UIkit.notification({
                message: 'La fila ha sido borrada',
                status: 'success',
                pos: 'top-center',
                timeout: 4000
            });
            
        }, function () {
            console.log('Rejected.');
        });

    }

    /**
     * Se validan los modos de falla del FMECA para definir si FMECA esta ok (Semaforo verde)
     * @returns {boolean}
     */
    validateMatrix(): boolean {

        for (let l = 0; l < this.currentNode.parts.length; l++) {
            const part = this.currentNode.parts[l];

            // Si la parte no tiene modos de falla, advertir problema
            part.fmeca.fmeca_status = part.fmeca.data.length > 0;
            part.fmeca.rcm_status = part.fmeca.data.length > 0;


            for (let i = 0; i < part.fmeca.data.length; i++) {
                const row = part.fmeca.data[i];

                for (let j = 0; j < this.moduleFields.length; j++) {
                    const field = this.moduleFields[j];

                    if (field.children.length === 0) {
                        if (field.required && !row[field.key]) {

                            // Si falla un campo fmeca, falla rcm tambien
                            if (field.parent_module === 'fmeca') {
                                part.fmeca.fmeca_status = false;
                                part.fmeca.rcm_status = false;

                            }

                            if (field.parent_module === 'rcm') {
                                part.fmeca.rcm_status = false;
                            }

                        }
                    } else {
                        for (let k = 0; k < field.children.length; k++) {
                            const ffield = field.children[k];

                            if (ffield.required && !row[ffield.key]) {
                                // Si falla un campo fmeca, falla rcm tambien
                                if (ffield.parent_module === 'fmeca') {
                                    part.fmeca.fmeca_status = false;
                                    part.fmeca.rcm_status = false;
                                }

                                if (ffield.parent_module === 'rcm') {
                                    part.fmeca.rcm_status = false;
                                }
                            }
                        }
                    }
                }
            }
        }

        return true;
    }

    calculateRowRisk(row): number {

        const frequency = this.getMaxFactorFromFields('frequency', row);
        const consequence = this.getMaxFactorFromFields('consequence', row);
        const detection = this.getMaxFactorFromFields('detection-method', row);

        // Si falta cualquier factor de riesgo, se borra el resultado para efecto de validaciones
        if (frequency === null || consequence === null || detection === null) {
            delete row.risk; // Simplemente se borra la variable
            return -1;
        }

        row.risk = frequency * consequence * detection;

        return row.risk;
    }

    getMaxFactorFromFields(key, row): number {
        let factor = null;

        for (let i = 0; i < this.moduleFields.length; i++) {
            const field = this.moduleFields[i];

            if (field.key === key) {

                if (field.children.length > 0) {
                    for (let j = 0; j < field.children.length; j++) {
                        const ffield = field.children[j];
                        const options = this.riskMatrix[ffield.options];


                        for (let k = 0; k < options.data.length; k++) {
                            const option = options.data[k];

                            if (row[ffield.key] && row[ffield.key] === option.level) { // TODO: return to option.id

                                if (factor === null) {
                                    factor = option.factor;
                                } else {
                                    factor = Math.max(factor, option.factor);
                                }
                                break;

                            }
                        }

                    }
                } else {

                    const options = this.riskMatrix[field.options];
                    for (let k = 0; k < options.data.length; k++) {
                        const option = options.data[k];

                        if (row[field.key] && row[field.key] === option.level) {

                            factor = option.factor;
                            break;
                        }
                    }
                }


                break;
            }


        }

        return factor;
    }

    s2ab(s: string): ArrayBuffer {
        const buf: ArrayBuffer = new ArrayBuffer(s.length);
        const view: Uint8Array = new Uint8Array(buf);
        for (let i = 0; i !== s.length; ++i) {
            view[i] = s.charCodeAt(i) & 0xFF;
        }
        return buf;
    }

    /**
     * Necesario pues actualizaciones mediante jquery no actualizaban modelo
     * @param model modelo actual
     * @param e evento
     */
    onDateChange(model, e) {
        model.date_modified = e.target.value;
    }

    setupDatepickerFields() {

        // Esperar update de DOM para aplicar selector de fecha/hora
        setTimeout(() => {
            $('.datetimepicker:not(.datetimepicker-initialized)')
                .addClass('datetimepicker-initialized')
                .datetimepicker({
                    timepicker: true,
                    format: 'Y-m-d H:i:s'
                });
        }, 500);
    }

    applyUserPreferences() {
        const userPreferences = this.dataService.getUserPreferences();

        // Se muestran/ocultan campos segun lo definido en Risk Matrix
        const rkmPreferences = userPreferences['rkm'];

        if (rkmPreferences) {

            this.moduleFields.forEach(e => {
                if (typeof rkmPreferences[e.key] !== 'undefined') {

                    if (e.children.length === 0) {

                        e.visible = rkmPreferences[e.key].visible;
                        e.required = rkmPreferences[e.key].required;

                    } else {
                        e.children.forEach(ee => {

                            const eeKey = ee.key.replace(e.key + '-', '');
                            if (typeof rkmPreferences[e.key][eeKey] !== 'undefined') {
                                ee.visible = rkmPreferences[e.key][eeKey].visible;
                                ee.required = rkmPreferences[e.key][eeKey].required;
                            }

                        });
                    }
                }
            });
        }
    }

    getVisibleChildren(field) {
        return field.children.filter(e => e.visible === true);
    }

    drawDecisionTree() {
        const that = this;
        this.oc = $('#orgchart').orgchart({
            'data': that.datasource,
            'nodeContent': 'content',
            'pan': true,
            'zoom': true,
            'createNode': function ($node, data) {

                if (data.id === 1) {
                    $node.addClass('root-node');
                } else if (data.children.length === 0) {
                    $node.addClass('leaf-node');
                }
            }
        });
        this.oc.$chartContainer.find('.orgchart').addClass('noncollapsable');

        let $this;
        this.oc.$chartContainer.on('click', '.node', function () {
            $this = $(this);
            $('#oc-node-id').val($this.attr('id'));
            $('#oc-node-title').val($this.find('.title').text()).data('node', $this);
            $('#oc-node-content').val($this.find('.content').text()).data('node', $this);
        });


        // Borra guarda cambios del nodo seleccionado
        $('#oc-save-node').click(function () {

            const id = parseInt($('#oc-node-id').val(), 10);
            const title = $('#oc-node-title').val();
            const content = $('#oc-node-content').val();

            // Se busca y actualiza el nodo
            const node = that.ocFindNode(that.datasource, id);
            node.name = title;
            node.content = content;

            // Se dibuja el arbol con los datos actualizados
            that.oc.init(that.datasource);

        });

        // Borra el nodo seleccionado
        $('#oc-delete-node').click(function () {

            const id = parseInt($('#oc-node-id').val(), 10);
            const parentId = parseInt($('#orgchart #' + id).attr('data-parent'), 10);
            if (isNaN(parentId)) {

                UIkit.notification({
                    message: 'El nodo raíz no puede ser eliminado',
                    status: 'warning',
                    pos: 'top-center',
                    timeout: 4000
                });

                return;
            }


            UIkit.modal.confirm('Está seguro que desea borrar este nodo?').then(function () {

                const node = that.ocFindNode(that.datasource, parentId);
                node.children = node.children.filter(e => e.id !== id);

                // Se dibuja el arbol con los datos actualizados
                that.oc.init(that.datasource);

            }, function () {
                console.log('Rejected.');
            });
        });
    }


    ocSaveStructure() {
        this.dataService.saveDecisionTreeStructure(this.datasource)
            .subscribe(data => {
                UIkit.notification({
                    message: 'El árbol ha sido guardado exitósamente',
                    status: 'success',
                    pos: 'top-center',
                    timeout: 4000
                });
            });
    }

    // Agrega hijo al nodo seleccionado
    addOcNodeChild() {

        // Se prepara el nuevo nodo y se agrega
        const parentId = parseInt($('#oc-node-id').val(), 10);

        if (!isNaN(parentId)) {
            const id = this.ocGetMaxId() + 1;
            const title = 'Nuevo Título';
            const content = 'Nuevo Contenido';

            const node = this.ocFindNode(this.datasource, parentId);
            const newNode = {
                id: id,
                name: title,
                content: content,
                children: []
            };

            if (node.children) {
                node.children.push(newNode);
            } else {
                node.children = [newNode];
            }

            // Se dibuja el arbol con los datos actualizados
            this.oc.init(this.datasource);
        }

    }

    ocFindNode(rootNode, nodeId) {

        if (rootNode.id === nodeId) {
            return rootNode;
        }

        let nodes = [].concat(this.datasource.children);

        while (nodes.length > 0) {

            const e = nodes.pop();
            if (e.id === nodeId) {
                return e;
            } else {
                if (e.children) {
                    nodes = nodes.concat(e.children);
                }
            }
        }

        return null;
    }

    ocGetMaxId(): number {
        let maximum = 0;

        $('#orgchart .node').each(function () {
            const value = parseInt($(this).attr('id'), 10);
            maximum = (value > maximum) ? value : maximum;
        });

        return maximum;
    }

    toggleOcTree() {
        this.ocTreeVisible = !this.ocTreeVisible;
        this.ocTreeMaximized = false;
        this.oc.init(this.datasource);
    }

    toggleMaximizeOcTree() {
        this.ocTreeMaximized = !this.ocTreeMaximized;

        if (this.ocTreeMaximized) {
            const ocWidth = $('#orgchart .orgchart').width();
            let ocMatrix = $('#orgchart .orgchart').css('transform');
            if (ocMatrix === 'none') {
                ocMatrix = 'matrix(1,0,0,1,0,0)';
            }

            const ocMatrixParts = ocMatrix.split(',');
            ocMatrixParts[4] = ($(window).width() - ocWidth) / 2;

            $('#orgchart .orgchart').css('transform', ocMatrixParts.join(','));

        } else {
            this.oc.init(this.datasource);
        }
    }
}
