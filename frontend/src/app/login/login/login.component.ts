import {Component, OnInit} from '@angular/core';
import {DataService} from '../../shared/services/data.service';
import {Router} from '@angular/router';

declare var $: any;
declare var UIkit: any;

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    constructor(private router: Router, private dataService: DataService) {
    }

    ngOnInit() {
        const that = this;

        this.dataService.destroySession();

        // Inicializar validationEngine
        // $('form').validationEngine({promptPosition: 'bottomLeft', scroll: false});

        // Traer la imagen de fondo
        $.ajax({
            url: './assets/img/login-background-1.jpg',
            type: 'GET',
            mimeType: 'text/plain; charset=x-user-defined'
        }).done(function (data, textStatus, jqXHR) {
            $('#login-background').hide().css('background-image',
                'url(data:image/jpeg;base64,' + that.base64Encode(data) + ')').fadeIn('slow');
        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert('fail: ' + errorThrown);
        });
    }

    doLogin() {
        const username = $('#username').val();
        const password = $('#password').val();

        this.dataService.doLogin(username, password)
            .subscribe(data => {

                if (data['status']) {
                    this.dataService.setUser(data['user']);
                    this.router.navigate(['risk-matrix']);
                } else {
                    UIkit.notification({
                        message: 'No se encontró esa combinación de Usuario/Contraseña',
                        status: 'danger',
                        pos: 'top-center',
                        timeout: 4000
                    });
                }

            });

    }

    base64Encode(str) {
        const CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
        let out = '', i = 0, len = str.length, c1, c2, c3;
        while (i < len) {
            c1 = str.charCodeAt(i++) & 0xff;
            if (i == len) {
                out += CHARS.charAt(c1 >> 2);
                out += CHARS.charAt((c1 & 0x3) << 4);
                out += '==';
                break;
            }
            c2 = str.charCodeAt(i++);
            if (i == len) {
                out += CHARS.charAt(c1 >> 2);
                out += CHARS.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
                out += CHARS.charAt((c2 & 0xF) << 2);
                out += '=';
                break;
            }
            c3 = str.charCodeAt(i++);
            out += CHARS.charAt(c1 >> 2);
            out += CHARS.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
            out += CHARS.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6));
            out += CHARS.charAt(c3 & 0x3F);
        }
        return out;
    }

}
