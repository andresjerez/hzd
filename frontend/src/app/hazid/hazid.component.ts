import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../shared/services/data.service';
import { DxDataGridComponent } from 'devextreme-angular';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';


declare var $: any;
declare var UIkit: any;

@Component({
  selector: 'app-hazid',
  templateUrl: './hazid.component.html',
  styleUrls: ['./hazid.component.css']
})

export class HazidComponent implements AfterViewInit, OnInit {

  moduleInfo: any = { 'title': 'HAZID - Hazard and Identification', 'key': 'hazid' };
  isLoaded = false;
  riskDimensions = [];
  activeDimensionConsec: any = null;
  camposObligatorios = [['guide-word-id', 0], ['frequency', 0],['consequence-community', ""], ['consequence-environment', ""], ['consequence-finance', ""], ['consequence-legal', ""], ['consequence-reputation', ""], ['consequence-security', ""], ['detection', 0]];

  @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;

  applyFilterTypes: any;
  currentFilter: any;
  showFilterRow: boolean;
  showHeaderFilter: boolean;

  isNaN = (isNaN); // Se expone la funcion isNaN para su uso en template


  moduleFields: any = [
    {
      'id': 0,
      'key': 'id',
      'name': 'Id',
      'type': 'readonly',
      'visible': false,
      'parent_module': 'fmeca',
      'required': false,
      'children': []
    },
    {
      'id': 0,
      'key': 'function',
      'name': 'Función',
      'visible': true,
      'parent_module': 'fmeca',
      'required': false,
      'children': []
    },
    {
      'id': 0,
      'key': 'functional-failure',
      'name': 'Falla funcional',
      'visible': true,
      'parent_module': 'fmeca',
      'required': false,
      'children': []
    },
    {
      'id': 0,
      'key': 'failure-mode',
      'name': 'Modo de falla',
      'visible': true,
      'parent_module': 'fmeca',
      'required': true,
      'children': []
    },
    {
      'id': 0,
      'key': 'maintenable-element',
      'name': 'Elemento mantenible',
      'visible': true,
      'parent_module': 'fmeca',
      'required': true,
      'children': []
    },
    {
      'id': 0,
      'key': 'cause',
      'name': 'Causa',
      'visible': true,
      'parent_module': 'fmeca',
      'required': false,
      'children': []
    },
    {
      'id': 0,
      'key': 'effect',
      'name': 'Efecto',
      'visible': true,
      'parent_module': 'fmeca',
      'children': [
        {
          'id': 0,
          'key': 'effect-inmediate',
          'name': 'Inmediato',
          'visible': true,
          'parent_module': 'fmeca',
          'required': true
        },
        {
          'id': 0,
          'key': 'effect-asset',
          'name': 'Equipo',
          'visible': true,
          'parent_module': 'fmeca',
          'required': true
        },
        {
          'id': 0,
          'key': 'effect-process',
          'name': 'Proceso',
          'visible': true,
          'parent_module': 'fmeca',
          'required': true
        }
      ]
    },
    {
      'id': 0,
      'key': 'symptom',
      'name': 'Síntoma',
      'visible': true,
      'parent_module': 'fmeca',
      'required': false,
      'children': []
    },
    {
      'id': 0,
      'key': 'detection-method',
      'name': 'Método de detección',
      'visible': true, 'parent_module': 'fmeca', 'required': true,
      'type': 'select',
      'children': [],
      'options': 'risk-matrix_detection-detection'
    },
    {
      'id': 0,
      'key': 'evident',
      'name': 'Evidente', 'visible': true, 'parent_module': 'fmeca', 'required': true,
      'type': 'select-simple',
      'options': [
        { 'value': false, 'name': 'No' },
        { 'value': true, 'name': 'Si' }
      ],
      'children': []
    },
    {
      'id': 0,
      'key': 'frequency',
      'name': 'Frecuencia', 'visible': true, 'parent_module': 'fmeca', 'required': true,
      'children': [],
      'type': 'select',
      'options': 'risk-matrix_frequency-frequency'
    },
    {
      'id': 0,
      'key': 'consequence',
      'name': 'Consecuencia',
      'type': 'select', 'visible': true, 'parent_module': 'fmeca', 'required': true,
      'options': 'risk-matrix_consequence',
      'children': [
        {
          'id': 0,
          'key': 'consequence-security',
          'name': 'Seguridad',
          'type': 'select', 'visible': true, 'parent_module': 'fmeca', 'required': true,
          'options': 'risk-matrix_consequence-security'
        },
        {
          'id': 0,
          'key': 'consequence-environment',
          'name': 'Medio ambiente',
          'type': 'select', 'visible': true, 'parent_module': 'fmeca', 'required': true,
          'options': 'risk-matrix_consequence-environment'
        },
        {
          'id': 0,
          'key': 'consequence-community',
          'name': 'Comunidad',
          'type': 'select', 'visible': true, 'parent_module': 'fmeca', 'required': true,
          'options': 'risk-matrix_consequence-community'
        },
        {
          'id': 0,
          'key': 'consequence-reputation',
          'name': 'Reputación',
          'type': 'select', 'visible': true, 'parent_module': 'fmeca', 'required': true,
          'options': 'risk-matrix_consequence-reputation'
        },
        {
          'id': 0,
          'key': 'consequence-legal',
          'name': 'Legal',
          'type': 'select', 'visible': true, 'parent_module': 'fmeca', 'required': true,
          'options': 'risk-matrix_consequence-legal'
        },
        {
          'id': 0,
          'key': 'consequence-finance',
          'name': 'Financiera',
          'type': 'select', 'visible': true, 'parent_module': 'fmeca', 'required': true,
          'options': 'risk-matrix_consequence-finance'
        }
      ]
    },
    {
      'id': 0,
      'key': 'detection',
      'name': 'Deteccion', 'visible': true, 'parent_module': 'fmeca', 'required': true,
      'children': [],
      'type': 'select',
      'options': 'risk-matrix_detection-detection'
    },
    {
      'id': 0,
      'key': 'risk',
      'name': 'Riesgo',
      'type': 'readonly',
      'visible': true,
      'parent_module': 'fmeca',
      'required': true,
      'description': 'Riesgo es el resultado de la multiplicación de los mayores ' +
        'Factores de riesgo en los grupos Frecuencia, Consecuencia y Método de detección',
      'children': []
    },
    {
      'id': 0,
      'key': 'strategy',
      'name': 'Estrategia',
      'type': 'select-simple',
      'visible': false, 'parent_module': 'rcm', 'required': true,
      'children': [],
      'options': [{ 'value': 'preventive', 'name': 'Preventivo' },
      { 'value': 'condition', 'name': 'Condición' },
      { 'value': 'corrective', 'name': 'Correctivo' },
      { 'value': 'redesign', 'name': 'Rediseño' },
      { 'value': 'inspection-hidden-failures', 'name': 'Insp. fallas ocultas' },
      { 'value': 'preventive-condition', 'name': 'Preventivo & Condición' },
      { 'value': 'inspection-hidden-failures-preventive', 'name': 'Insp. fallas ocultas & Preventivo' },
      { 'value': 'inspection-hidden-failures-condition', 'name': 'Insp. fallas ocultas & Condición' }]
    },
    {
      'id': 0,
      'key': 'control-method',
      'name': 'Método de control',
      'type': 'text',
      'visible': false, 'parent_module': 'rcm', 'required': true,
      'children': []
    },
    {
      'id': 0,
      'key': 'control-frequency',
      'name': 'Frecuencia de control',
      'type': 'number',
      'visible': false, 'parent_module': 'rcm', 'required': true,
      'children': []
    },
    {
      'id': 0,
      'key': 'frequency-unit',
      'name': 'Unidad de frecuencia',
      'type': 'select-simple',
      'visible': false, 'parent_module': 'rcm', 'required': true,
      'children': [],
      'options': [
        { 'value': 'hour', 'name': 'Horas' },
        { 'value': 'day', 'name': 'Días' },
        { 'value': 'month', 'name': 'Meses' },
        { 'value': 'year', 'name': 'Años' },
        { 'value': 'hours-op', 'name': 'Horas op.' },
        { 'value': 'cycle', 'name': 'Ciclos' },
        { 'value': 'tkph', 'name': 'tkph' }
      ]
    },


    // maintenance plan
    {
      'id': 0,
      'key': 'activity-plan',
      'name': 'Actividad plan',
      'type': 'select-simple',
      'visible': false,
      'parent_module': 'maintenance-plan', 'required': false,
      'options': [
        { 'value': 'pm', 'name': 'PM' },
        { 'value': 'inspection', 'name': 'Inspección' },
        { 'value': 'part-change', 'name': 'Cambio de parte' },
        { 'value': 'adjustment', 'name': 'Ajuste' }
      ],
      'children': []
    },
    {
      'id': 0,
      'key': 'description',
      'name': 'Descripción',
      'type': 'text',
      'visible': false,
      'parent_module': 'maintenance-plan', 'required': false,
      'children': []
    },
    {
      'id': 0,
      'key': 'specialty',
      'name': 'Especialidad',
      'type': 'select-simple',
      'visible': false,
      'parent_module': 'maintenance-plan', 'required': false,
      'options': [
        { 'value': 'mechanical', 'name': 'Mecánico' },
        { 'value': 'electrical', 'name': 'Eléctrico' },
        { 'value': 'instrumentation', 'name': 'Instrumentación' },
        { 'value': 'control', 'name': 'Control' },
        { 'value': 'lubrication', 'name': 'Lubricación' },
        { 'value': 'vulcanization', 'name': 'Vulcanización' }
      ],
      'children': []
    },
    {
      'id': 0,
      'key': 'plan-frequency',
      'name': 'Frecuencia plan',
      'type': 'number',
      'visible': false,
      'parent_module': 'maintenance-plan', 'required': false,
      'children': []
    },
    {
      'id': 0,
      'key': 'plan-frequency-unit',
      'name': 'Unidad frecuencia plan',
      'type': 'select-simple',
      'visible': false,
      'parent_module': 'maintenance-plan', 'required': false,
      'options': [
        { 'value': 'hour', 'name': 'Horas' },
        { 'value': 'day', 'name': 'Días' },
        { 'value': 'month', 'name': 'Meses' },
        { 'value': 'year', 'name': 'Años' },
        { 'value': 'hours-op', 'name': 'Horas op.' },
        { 'value': 'cycle', 'name': 'Ciclos' },
        { 'value': 'tkph', 'name': 'tkph' }
      ],
      'children': []
    },
    {
      'id': 0,
      'key': 'hh-plan',
      'name': 'HH plan',
      'type': 'number',
      'visible': false,
      'parent_module': 'maintenance-plan', 'required': false,
      'children': []
    },
    {
      'id': 0,
      'key': 'staff',
      'name': 'Dotación',
      'type': 'number',
      'visible': false,
      'parent_module': 'maintenance-plan', 'required': false,
      'children': []
    },
    {
      'id': 0,
      'key': 'duration',
      'name': 'Duración',
      'type': 'number',
      'visible': false,
      'parent_module': 'maintenance-plan', 'required': false,
      'children': []
    },
    {
      'id': 0,
      'key': 'parts',
      'name': 'Repuestos',
      'type': 'text',
      'visible': false,
      'parent_module': 'maintenance-plan', 'required': false,
      'children': []
    },
    {
      'id': 0,
      'key': 'tools',
      'name': 'Herramientas',
      'type': 'text',
      'visible': false,
      'parent_module': 'maintenance-plan', 'required': false,
      'children': []
    },
    {
      'id': 0,
      'key': 'supplies',
      'name': 'Insumos',
      'type': 'text',
      'visible': false,
      'parent_module': 'maintenance-plan', 'required': false,
      'children': []
    },
    {
      'id': 0,
      'key': 'support-assets',
      'name': 'Equipos de apoyo',
      'type': 'text',
      'visible': false,
      'parent_module': 'maintenance-plan', 'required': false,
      'children': []
    }
  ];

  riskMatrix: any = {};

  isNewFormVisible = false;


  currentNode: any = null;
  currentPart: any = null;
  changedNodes: any = [];
  tempNodeId = 0;

  frequency: any = null;

  tableSelects: any = {
    frequency_frequency: null,
    consequence_environment: null,
    consequence_finance: null,
    consequence_legal: null,
    consequence_reputation: null,
    consequence_security: null,
    consequence_community: null,
    detection_detection: null,
    confirmation: null

  };


  newPart: any = null;
  partTypes: any = null;

  hazidCategories = null;
  currentPartId = 0;

  treeContainerId = '#cgs-dlf';

  editEnabled: boolean;

  filteredWords: any = null;

  constructor(private dataService: DataService) {
    this.editEnabled = this.dataService.userCanEdit();
    this.showFilterRow = true;
    this.showHeaderFilter = true;
    this.applyFilterTypes = [{
      key: "auto",
      name: "Immediately"
    }, {
      key: "onClick",
      name: "On Button Click"
    }];

    this.getFilteredGuideWords = this.getFilteredGuideWords.bind(this);
  }

  observables: any;

  ngOnInit() {

    this.applyUserPreferences();



    // Se obtienen los datos desde backend
    this.observables = Observable.forkJoin(
      this.dataService.getRiskMatrixOptions(),
      this.dataService.getHazidCategories(),
      this.dataService.getPartTypes()
    );

    this.observables.subscribe((data) => {

      this.riskMatrix = data[0]['risk_matrix_options'];
      this.fillSelects(this.riskMatrix);


      if (data[1].status) {
        this.hazidCategories = data[1].hazid_categories;

        let filteredWords = [];
        this.hazidCategories.forEach(category => {
          filteredWords = filteredWords.concat(category.guide_words);
        });
        this.filteredWords = filteredWords;
      }

      if (data[2].status) {
        this.partTypes = data[2].part_types;
      }

      this.isLoaded = true;
      this.getCompanyTree();

    },
      () => { },
      () => { }
    );

    const req = this.dataService.getRiskMatrixDimensions()
    .subscribe(data => {
        this.riskDimensions = data['dimensions'];
        this.activeDimensionConsec = this.riskDimensions[1];
    });

  }
  export(){
    this.dataGrid.instance.exportToExcel(false);
  }
  fillSelects(data) {

    this.tableSelects.frequency_frequency = data['risk-matrix_frequency-frequency'].data;
    this.tableSelects.consequence_community = data['risk-matrix_consequence-community'].data;
    this.tableSelects.consequence_environment = data['risk-matrix_consequence-environment'].data;
    this.tableSelects.consequence_finance = data['risk-matrix_consequence-finance'].data;
    this.tableSelects.consequence_legal = data['risk-matrix_consequence-legal'].data;
    this.tableSelects.consequence_reputation = data['risk-matrix_consequence-reputation'].data;
    this.tableSelects.consequence_security = data['risk-matrix_consequence-security'].data;
    this.tableSelects.detection_detection = data['risk-matrix_detection-detection'].data;
    this.tableSelects.confirmation = [{ description: "No", value: "false" }, { description: "Sí", value: "true" }];

  }

  onSelectionChanged(e, dropDownBoxInstance) {
    const keys = e.selectedRowKeys;
    dropDownBoxInstance.option('value', keys.length > 0 ? keys[0] : null);
  }

  onValueChanged(args, setValueMethod) {
    setValueMethod(args.value);
  }

  initColumns() {

    for (let i = 0; i < this.moduleFields.length; i++) {
      const field = this.moduleFields[i];
      this.hideColumn(field.id);
    }

  }

  hideColumn(columnName) {
    this.dataGrid.instance.columnOption(columnName, 'visible', false);
  }

  setCellValue(newData, value, currentRowData) {
    console.log("Ok");
  }

  updateRisk() {
    for (let i = 0; i < this.currentPart.hazid.data.length; i++) {
      const row = this.currentPart.hazid.data[i];
      this.calculateRowRisk(row);
    }

  }



  ngAfterViewInit() {
    $.datetimepicker.setLocale('es');
  }

  /**
   * Se obtienen los nodos del DLF y luego se genera el arbol
   */
  getCompanyTree() {
    this.dataService.getCompanyTreeStructure()
      .subscribe(data => this.generateCompanyTree(data, this.treeContainerId));
  }

  /**
   * Se genera el arbol DLF y se configuran sus eventos
   * @param tree arbol de nodos del DLF
   * @param selector selector DOM donde se dibujara el arbol
   */
  generateCompanyTree(tree, selector) {

    const IMG_URL = './assets/img';
    $(selector).jstree('destroy');
    $.jstree.defaults.checkbox.three_state = false;
    const parent = this;
    const treeTypes = {
      default: {
        icon: IMG_URL + '/jstree_icons/Search.png'
      },
      graph: {
        icon: IMG_URL + '/jstree_icons/corporation.png'
      },
      area: {
        icon: IMG_URL + '/jstree_icons/equipPlant.png'
      },
      linearmachine: {
        icon: IMG_URL + '/jstree_icons/line.png'
      },
      equipment: {
        icon: IMG_URL + '/jstree_icons/equip.png'
      },
      standbymachine: {
        icon: IMG_URL + '/jstree_icons/standby.png'
      },
      fractionmachine: {
        icon: IMG_URL + '/jstree_icons/fraction.png'
      },
      redundantmachine: {
        icon: IMG_URL + '/jstree_icons/redundant.png'
      },
      parallelmachine: {
        icon: IMG_URL + '/jstree_icons/parallel.png'
      },
      stockpile: {
        icon: IMG_URL + '/jstree_icons/stockpile.png'
      }
    };
    // Crear una instancia de js tree
    //noinspection TypeScriptUnresolvedFunction
    $(selector).jstree({
      'core': {
        data: tree,
        check_callback: true
      },
      types: treeTypes,
      state: { key: 'rmes-suite' },
      plugins: ['search', 'types', 'state']
    });

    const companyTree = $(selector).jstree(true);
    let to = null;
    $('#tree-search').keyup(function () {
      if (to) {
        clearTimeout(to);
      }
      to = setTimeout(function () {
        const v = $('#tree-search').val();
        if (v.length >= 3 || v.length === 0) {
          $(selector).jstree(true).search(v);
        }
      }, 250);
    });
    // Listener para click en elementos de jstree
    $(selector).on('changed.jstree', function (e, data) {
      const nodes = companyTree.get_selected(true);


      if (nodes.length > 0 && nodes[0].data.isleaf === '1') { // TODO: Clean json input
        parent.currentNode = nodes[0];
        parent.configureNode();
      }

    });




    // Listener para reseleccionar nodo tras un reload
    $(selector).on('ready.jstree', function (e, data) {
      if (parent.tempNodeId !== 0) {

        //const nodes = $(selector).jstree(true).get_json('#', { flat: true });
     
        //for (let i = 0; i < nodes.length; i++) {
          //const n = nodes[i];

          //if (n.data.subsystem_ref === parent.tempNodeId) {

            var id_padre = companyTree.element["0"].childNodes["0"].childNodes["0"].id;
            id_padre = id_padre.split("_");
            var id_hijo = parent.tempNodeId.toString();
            var id_hijo2 = id_hijo.split("_");
            var id_final_hijo = id_padre[0]+"_"+id_hijo2[1];
            $(selector).jstree().select_node(id_final_hijo);

            //break;
          //}
        //}
      }
    });
  }

  configureNode() {

    this.currentPart = null;
    if (typeof this.currentNode.parts === 'undefined') {
      this.getSubsystemParts(this.currentNode);
    }
    const parent = $(this.treeContainerId).jstree().get_node(this.currentNode.parent);

    const nodeIndex = this.changedNodes.indexOf(this.currentNode);
    if (nodeIndex === -1) {
      this.changedNodes.push(this.currentNode);
    } else {
      this.changedNodes[nodeIndex] = this.currentNode;
    }

    this.currentNode.parent = parent;
  }

  getSubsystemParts(node) {

    const subsystemId = node.data.subsystem_ref;

    if (subsystemId) {
      const treeData = this.dataService.getSubsystemParts(subsystemId)
        .subscribe(data => {

          // Se añade el campo type para facilidad de uso en tabla
          data['parts'].forEach(part => {

            if (part.type_id) {
              const partType = this.partTypes.find(pt => parseInt(pt.id, 10) === parseInt(part.type_id, 10));
              part.type = (partType) ? partType : null;
            }

            if (this.currentPartId > 0 && part.id === this.currentPartId){
              this.currentPart = part;
            }
          });


          node.parts = data['parts'];
        });
    }
  }

  onSelectPart(part) {
    this.currentPart = part;
    this.updateRisk();
  }

  createPart() {

    this.newPart = {
      'id': null,
      'subsystem_id': this.currentNode.data.subsystem_ref,
      'name': 'Parte #' + (this.currentNode.parts.length + 1),
      'type': null,
      'hazid': {
        'hazid_status': false,
        'status': 1,
        'data': []
      },
      'version': '0.1',
      'date_modified': '',
      'status': 1
    };

    this.setupDatepickerFields();
    UIkit.modal('#part-new-modal').show();

  }

  addPart() {

    if (this.newPart.type) {
      this.newPart.type_id = this.newPart.type.id;
    }

    this.currentNode.parts.push(this.newPart);

    // Focus on the new part
    $('#cgs-fmeca-navigator').animate({ scrollTop: $('#cgs-fmeca-navigator table').height() }, 100);
    this.onSelectPart(this.currentNode.parts[this.currentNode.parts.length - 1]);
   
    UIkit.notification({
      message: 'La parte ha sido agregada',
      status: 'success',
      pos: 'top-center',
      timeout: 4000
    });

    UIkit.modal('#part-new-modal').hide();
    this.saveChanges();
  }

  editPart(part) {

    this.setupDatepickerFields();
    UIkit.modal('#part-edit-modal').show();

  }

  savePart() {

    $("#part-edit-modal").css('display','none');
    // Si se cambia el tipo de componente, los campos accion y parametros de sus registros se resetean (afecta a hazop)
    if (this.currentPart.type) {

      if (parseInt(this.currentPart.type_id, 10) !== parseInt(this.currentPart.type.id, 10)) {
        this.currentPart.hazop.data.forEach(e => {
          e['parameter-id'] = 0;
          e['action-id'] = 0;
        });

        this.currentPart.type_id = this.currentPart.type.id;
      }
      this.saveChanges();

    }

    UIkit.notification({
      message: 'La parte ha sido actualizada',
      status: 'success',
      pos: 'top-center',
      timeout: 4000
    });
    UIkit.modal('#part-edit-modal').hide();

  }

  deletePart(part) {
    const parent = this;
    UIkit.modal.confirm('Está seguro que desea borrar la parte <b>' + part.name + '</b>').then(function () {
      part.status = 0;
      parent.currentPart = null;
      UIkit.notification({
        message: 'La parte ha sido borrada',
        status: 'success',
        pos: 'top-center',
        timeout: 4000
      });

      parent.saveDelete();

    }, function () {
      console.log('Rejected.');
    });
  }

  saveDelete() {

    this.dataGrid.instance.saveEditData();
    this.dataService.deleteHazid(this.changedNodes).subscribe(() => {

      // Se guarda el nodo actual para reabrirlo luego de recargar
      this.tempNodeId = this.currentNode.id;
      this.currentPartId = this.currentPart.id;

      // Recargar datos para mantener consistencia
      this.currentNode = null;
      this.currentPart = null;
      this.changedNodes = [];
      this.getCompanyTree();

      UIkit.notification({
        message: 'Cambios guardados exitósamente',
        status: 'success',
        pos: 'top-center',
        timeout: 4000
      });
    });
  }

  getPartTypeName(typeId) {

    if (isNaN(typeId)) { return null; }

    typeId = parseInt(typeId, 10);
    const partType = this.partTypes.find(pt => pt.id === typeId);

    return partType ? partType.name : null;
  }

  saveChanges() {

    /* traspasar cambios en tipo de parte a id
    this.changedNodes.forEach(cn => {
      cn.parts.forEach(part => {
        if (typeof part.type !== 'undefined') {
          part.type_id = part.type.id;
        }
      });
    });*/

    this.dataGrid.instance.saveEditData();
    
    if(this.campoObligatorio(this.camposObligatorios) == false){
      return;
    }

    this.dataService.saveHazid(this.changedNodes).subscribe(() => {

      // Se guarda el nodo actual para reabrirlo luego de recargar
      this.tempNodeId = this.currentNode.id;
      this.currentPartId = this.currentPart.id;
      // Recargar datos para mantener consistencia
      this.currentNode = null;
      this.currentPart = null;
      this.changedNodes = [];
      this.getCompanyTree();

      UIkit.notification({
        message: 'Cambios guardados exitósamente',
        status: 'success',
        pos: 'top-center',
        timeout: 4000
      });
    });
  }

  addHazid() {
    this.currentPart.hazid.data.push({ 'id': null, status: 1 });
    this.dataGrid.instance.refresh();
  }

  campoObligatorio(campo) {

    this.camposObligatorios = [['guide-word-id', 0], ['frequency', 0],['consequence-community', ""], ['consequence-environment', ""], ['consequence-finance', ""], ['consequence-legal', ""], ['consequence-reputation', ""], ['consequence-security', ""], ['detection', 0]];

    for (let i = 0; i < this.moduleFields.length; i++) {
      if (this.moduleFields[i].key == "consequence") {
        var index = i;
      }
    }
    
    for (let i = 2; i < (this.camposObligatorios.length - 1); i++) {
      var cuenta = 0;
      var indexCampo = i;
      for (let k = 0; k < this.moduleFields[index].children.length; k++) {
        if (this.moduleFields[index].children[k].key != this.camposObligatorios[i][0]) {
          cuenta = cuenta + 1;
        }
      }

      if (cuenta == (this.moduleFields[index].children.length)) {
          this.camposObligatorios.splice(i, 1);
      }
    }

    for (let k = 0; k < this.changedNodes.length; k++) {
      for (let i = 0; i < this.changedNodes[k].parts.length; i++) {
        if (this.changedNodes[k].parts[i].hazid.data.length != 0) {
          for (let j = 0; j < this.changedNodes[k].parts[i].hazid.data.length; j++) {
            for (let n = 0; n < this.camposObligatorios.length; n++) {
              if (this.changedNodes[k].parts[i].hazid.data[j][campo[n][0]] == null || this.changedNodes[k].parts[i].hazid.data[j][campo[n][0]] == campo[n][1]) {
                UIkit.notification({
                  message: 'Tiene que completar los campos obligatorios de ' + this.changedNodes[k].parts[i].name + ' para guardar',
                  status: 'warning',
                  pos: 'top-center',
                  timeout: 4000
                });
                return false;
              }
            }          
          }
        }
      }
    }
    return true;
  }

  deleteRow(object) {
    const parent = this;
    UIkit.modal.confirm('Está seguro que desea borrar la fila </b>').then(function () {
      object.data.status = 0;
      UIkit.notification({
        message: 'La fila ha sido borrada',
        status: 'success',
        pos: 'top-center',
        timeout: 4000
      });

      parent.saveDelete();

    }, function () {
      console.log('Rejected.');
    });
  }

  deleteFailureMode(row) {

    const parent = this;
    UIkit.modal.confirm('Está seguro que desea borrar esta fila?').then(function () {
      row.status = 0;
      UIkit.notification({
        message: 'La fila ha sido borrada',
        status: 'success',
        pos: 'top-center',
        timeout: 4000
      });
    }, function () {
      console.log('Rejected.');
    });

  }

  /**
   * Se filtran las palabras guias por categoria
   * @param options variable entregada por dxdatagrid
   */
  getFilteredGuideWords(options) {
    return {
      store: this.filteredWords,
      filter: options.data ? ['category_id', '=', parseInt(options.data['category-id'], 10)] : null
    };
  }

  setGuideWordValue(rowData: any, value: any): void {
    rowData['guide-word-id'] = null;
    (<any>this).defaultSetCellValue(rowData, value);
  }

  onEditorPreparing(e) {
    if (e.parentType === 'dataRow' && e.dataField === 'guide-word-id') {
      e.editorOptions.disabled = !e.row.data['category-id'];
    }
  }

 

  calculateRowRisk(row): number {

    const frequency = this.getMaxFactorFromFields('frequency', row);
    const consequence = this.getMaxFactorFromFields('consequence', row);
    const detection = this.getMaxFactorFromFields('detection', row);

    // Si falta cualquier factor de riesgo, se borra el resultado para efecto de validaciones
    if (frequency === null || consequence === null || detection === null) {
      delete row.risk; // Simplemente se borra la variable
      return -1;
    }

    row.risk = frequency * consequence * detection;

    return row.risk;
  }

  getMaxFactorFromFields(key, row): number {
    let factor = null;

    for (let i = 0; i < this.moduleFields.length; i++) {
      const field = this.moduleFields[i];

      if (field.key === key) {

        if (field.children.length > 0) {

          for (let j = 0; j < field.children.length; j++) {
            if(field.children[j].visible == false){
              field.children.splice(j, 1);
            }
          }

          for (let j = 0; j < field.children.length; j++) {
            const ffield = field.children[j];
            const options = this.riskMatrix[ffield.options];


            for (let k = 0; k < options.data.length; k++) {
              const option = options.data[k];

              if (row[ffield.key] && row[ffield.key] == option.level) { // TODO: return to option.id

                if (factor === null) {
                  factor = option.factor;
                } else {
                  factor = Math.max(factor, option.factor);
                }
                break;

              }
            }

          }
        } else {

          const options = this.riskMatrix[field.options];
          for (let k = 0; k < options.data.length; k++) {
            const option = options.data[k];

            if (row[field.key] && row[field.key] === option.level) {

              factor = option.factor;
              break;
            }
          }
        }


        break;
      }


    }

    return factor;
  }

  /**
   * Necesario pues actualizaciones mediante jquery no actualizaban modelo
   * @param model modelo actual
   * @param e evento
   */
  onDateChange(model, e) {
    model.date_modified = e.target.value;
  }

  setupDatepickerFields() {

    // Esperar update de DOM para aplicar selector de fecha/hora
    setTimeout(() => {
      $('.datetimepicker:not(.datetimepicker-initialized)')
        .addClass('datetimepicker-initialized')
        .datetimepicker({
          timepicker: true,
          format: 'Y-m-d H:i:s'
        });
    }, 500);
  }

  applyUserPreferences() {
    const userPreferences = this.dataService.getUserPreferences();

    // Se muestran/ocultan campos segun lo definido en Risk Matrix
    const rkmPreferences = userPreferences['rkm'];

    if (rkmPreferences) {

      this.moduleFields.forEach(e => {
        if (typeof rkmPreferences[e.key] !== 'undefined') {

          if (e.children.length === 0) {

            e.visible = rkmPreferences[e.key].visible;
            e.required = rkmPreferences[e.key].required;

          } else {
            e.children.forEach(ee => {

              const eeKey = ee.key.replace(e.key + '-', '');
              if (typeof rkmPreferences[e.key][eeKey] !== 'undefined') {
                ee.visible = rkmPreferences[e.key][eeKey].visible;
                ee.required = rkmPreferences[e.key][eeKey].required;
              }

            });
          }
        }
      });
    }

  }

  getVisibleChildren(field) {
    return field.children.filter(e => e.visible === true);
  }
}
