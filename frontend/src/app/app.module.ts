

import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component, ViewChild, enableProdMode } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { RiskMatrixComponent } from './risk-matrix/risk-matrix.component';
import { FmecaComponent } from './fmeca/fmeca.component';
import { MaintenancePlanComponent } from './maintenance-plan/maintenance-plan.component';
import { RcmComponent } from './rcm/rcm.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { SplitPaneModule } from 'ng2-split-pane/lib/ng2-split-pane';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {
    DxDataGridComponent,
    DxDataGridModule,
    DxButtonModule,
    DxSelectBoxModule,
    DxCheckBoxModule,
    DxDropDownBoxModule,
    DxMenuModule
} from 'devextreme-angular';

import { DataService } from './shared/services/data.service';
import { GeneralInterceptorProvider } from './shared/services/general-interceptor';
import { LoaderService } from './shared/services/loader.service';
import { LoginComponent } from './login/login/login.component';
import { OAuthCallbackComponent } from './login-callback/oauth-callback.component';
import { OAuthCallbackHandler } from './login-callback/oauth-callback-handler.guard';
import { SharedServicesModule } from './shared/services/azure/shared.service';
import { HazopComponent } from './hazop/hazop.component';
import { HazidComponent } from './hazid/hazid.component';
import { SettingsComponent } from './settings/settings.component';
import { PartsComponent } from './settings/parts/parts.component';
import { GuideWordsComponent } from './settings/hazop/guide-words/guide-words.component';
import { HazidCategoriesComponent } from './settings/hazid/hazid-categories/hazid-categories.component';

const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },

    { path: 'risk-matrix', component: RiskMatrixComponent },
    { path: 'fmeca', component: FmecaComponent },
    { path: 'rcm', component: RcmComponent },
    { path: 'maintenance-plan', component: MaintenancePlanComponent },
    { path: 'hazop', component: HazopComponent },
    { path: 'hazid', component: HazidComponent },
    { path: 'settings', component: SettingsComponent },

    { path: 'id_token', component: OAuthCallbackComponent, canActivate: [OAuthCallbackHandler] },
    { path: '**', component: LoginComponent }
];

@NgModule({
    declarations: [
        AppComponent,
        RiskMatrixComponent,
        FmecaComponent,
        RcmComponent,
        MaintenancePlanComponent,
        LoginComponent,
        OAuthCallbackComponent,
        HazopComponent,
        HazidComponent,
        SettingsComponent,
        PartsComponent,
        GuideWordsComponent,
        HazidCategoriesComponent
    ],
    imports: [
        RouterModule.forRoot(
            appRoutes, { useHash: true }
            // {enableTracing: true}  <-- debugging purposes only
        ),
        HttpModule,
        HttpClientModule,
        BrowserModule,
        FormsModule,

        // Externals
        SplitPaneModule,
        DxDataGridModule,
        DxButtonModule,
        DxMenuModule,
        DxSelectBoxModule,
        DxCheckBoxModule,
        DxDropDownBoxModule,
        SharedServicesModule

    ],
    providers: [DataService, GeneralInterceptorProvider, LoaderService, OAuthCallbackHandler],
    bootstrap: [AppComponent]
})

export class AppModule {
    // constructor(private dataService: DataService) {
    //
    // }
}
