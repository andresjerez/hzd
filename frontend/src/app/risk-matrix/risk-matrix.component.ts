import { Component, OnInit } from '@angular/core';
import { DataService } from '../shared/services/data.service';

declare var UIkit: any;

@Component({
    selector: 'app-risk-matrix',
    templateUrl: './risk-matrix.component.html',
    styleUrls: ['./risk-matrix.component.css']
})

export class RiskMatrixComponent implements OnInit {

    moduleInfo: any = { 'title': 'Risk Matrix', 'key': 'rkm' };
    riskDimensions = [];
    activeDimension: any = null;
    statSend = false;
    isUsedData: any = [];

    editEnabled: boolean;

    constructor(private dataService: DataService) {
        this.editEnabled = this.dataService.userCanEdit();
    }

    ngOnInit() {

        const req = this.dataService.getRiskMatrixDimensions()
            .subscribe(data => {
                this.riskDimensions = data['dimensions'];
                this.activeDimension = this.riskDimensions[0];
                if (this.isUsedData != null ) {
                    this.isUsed();
                }
            });
        
        const req2 = this.dataService.getRiskMatrixUsed()
            .subscribe(data => {
                this.isUsedData = data;
                if (this.activeDimension != null ) {
                    this.isUsed();
                }
        });

    }

    isUsed() {
       
        for (let i = 0; i < this.activeDimension.matrix.length; i++) {
            for (let j = 0; j < this.activeDimension.matrix[i].data.length; j++) {
            
                this.activeDimension.matrix[i].data[j].use_count = 0;
            }
        }

        var keysRM = Object.keys(this.isUsedData); 

        for (let i = 0; i < keysRM.length; i++) {
            if (keysRM[i] == this.activeDimension.key) {
            
                var keys = Object.keys(this.isUsedData[keysRM[i]]);
                
                for (let j = 0; j < this.activeDimension.matrix.length; j++) {
                    for (let k = 0; k < this.activeDimension.matrix[j].data.length; k++) {
                        for (let m = 0; m < keys.length; m++) {
                            if (this.isUsedData[keysRM[i]][keys[m]] == this.activeDimension.matrix[j].data[k].level) {
                                this.activeDimension.matrix[j].data[k].use_count = 1;
                            }
                        }         
                    }   
                }
            }  
        }
    }

    addMatrixRow(): void {
        this.resetLevels();
        const newLevel = this.getLastLevel() + 1;

        for (let i = 0; i < this.activeDimension.matrix.length; i++) {
            const matrix = this.activeDimension.matrix[i];
            matrix.data.push({ id: null, level: newLevel, factor: 0, description: '', class: 'low', status: 1 });
        }
    }

    /**
     Actualiza los atributos cuyo valor no es 1 a 1 (nivel, factor y clase).
     Esto es necesario tras un cambio de ultima momento en el requerimiento
     TODO: considerar "girar" la tabla  o cambiar el modelo para que la vista/tabla calce mas naturalmente
     */
    updateRow(rowIndex, attribute, val) {
        for (let i = 0; i < this.activeDimension.matrix.length; i++) {
            const matrix = this.activeDimension.matrix[i];
            matrix.data[rowIndex][attribute] = val;
        }

        
    }

    deleteRow(rowIndex) {

        // No se permite borrar filas que estan en uso
        if (this.activeDimension.matrix[0].data[rowIndex].use_count > 0) {
            return;
        }

        const parent = this;
        UIkit.modal.confirm('Está seguro que desea eliminar la fila seleccionada?').then(function () {
            for (let i = 0; i < parent.activeDimension.matrix.length; i++) {
                const subdimension = parent.activeDimension.matrix[i];
                subdimension.data[rowIndex].status = 0;
                // const matrix = parent.activeDimension.matrix[i];
                // matrix.data.splice(rowIndex, 1);
            }
            
            parent.resetLevels();
            parent.saveChanges();
            UIkit.notification({
                message: 'La fila ha sido eliminada.',
                status: 'success',
                pos: 'top-center',
                timeout: 4000
            });

        }, function () {
            console.log('Delete rejected.');
        });

    }

    /**
     * Guarda todos los cambios y recarga las dimensiones para mantener consistencia
     */
    saveChanges() {
        this.resetLevels();
        const req = this.dataService.saveRiskMatrix(this.riskDimensions)
            .subscribe(
                res => {

                    if (res['status']) {
                        this.riskDimensions = res['dimensions'];
                        this.changeActiveMajorMatrix(this.activeDimension.key);
                        this.statSend = false;
                        UIkit.notification({
                            message: 'Los cambios se han guardado.',
                            status: 'success',
                            pos: 'top-center',
                            timeout: 4000
                        });
                    }

                },
                err => {

                    console.log('Error occured');

                }
            );
    }

    checkSubmit() {
        if (this.statSend == false) {
            this.statSend = true; 
            this.saveChanges();
        }
    }

    changeActiveMajorMatrix(dimension) {

        if (typeof dimension === 'string') {
            this.riskDimensions.forEach(e => {
                if (e.key === dimension) {
                    this.activeDimension = e;
                    return false;
                }
            });
            this.isUsed();
        } else {
            this.activeDimension = dimension;
            this.isUsed();
        }

        // Se muestran/ocultan campos segun las preferencias del usuario
        const userPreferences = this.dataService.getUserPreferences();
        if (userPreferences[this.moduleInfo.key]) {
            if (typeof userPreferences[this.moduleInfo.key][this.activeDimension.key] !== 'undefined') {

                this.activeDimension.matrix.forEach(e => {
                    const rowKey = userPreferences[this.moduleInfo.key][this.activeDimension.key][e.key];
                    e.visible = typeof rowKey !== 'undefined' ? rowKey.visible : true;
                });
            }
        }
    }

    getLastLevel(): number {
        let level = 0;

        this.activeDimension.matrix[0].data.forEach(function (e) {
            if (e.status) {
                level++;
            }
        });
        // level = this.activeDimension.matrix[0].data.length;

        return level;
    }

    /**
     Reinicia la variable level de cada fila activa en sus correspondientes dimensiones
     */
    resetLevels() {

        for (let i = 0; i < this.activeDimension.matrix.length; i++) {
            const matrix = this.activeDimension.matrix[i];

            let level = 1;
            for (let j = 0; j < matrix.data.length; j++) {
                if (matrix.data[j].status) {
                    this.activeDimension.matrix[i].data[j].level = level++;
                }
            }

        }
    }

    saveDimensionVisibility(key, element) {
        const moduleKey = this.moduleInfo.key;
        const newValue = element.target.checked;

        const newPreference = {
            [moduleKey]: {
                'consequence': {
                    [key]: { 'visible': newValue, 'required': newValue }
                }
            }
        };

        for (let i = 0; i < this.activeDimension.matrix.length; i++) {  
            if (this.activeDimension.matrix[i].key == key) {
                if (this.activeDimension.matrix[i].visible == true) {
                    
                    var datos = [key, 1];
                    const req = this.dataService.changeVisibility(datos)
                    .subscribe(
                        res => {
                            if (res['status']) {
                                UIkit.notification({
                                    message: 'Los cambios se han guardado.',
                                    status: 'success',
                                    pos: 'top-center',
                                    timeout: 4000
                                });
                            }
                        },
                        err => {

                            console.log('Error occured');

                        }
                    );                
                }

                else{
                    var datos = [key, 0];
                    const req = this.dataService.changeVisibility(datos)
                        .subscribe(
                            res => {
                                if (res['status']) {
                                    UIkit.notification({
                                        message: 'Los cambios se han guardado.',
                                        status: 'success',
                                        pos: 'top-center',
                                        timeout: 4000
                                    });
                                }
                            },
                            err => {

                                console.log('Error occured');

                            }
                        );
                }
            }          
        }
         
        this.dataService.saveUserPreferences(newPreference);
        console.log('user preference saved');
      
    }
}
