import { TestBed, async, inject } from '@angular/core/testing';

import { OauthCallbackHandlerGuard } from './oauth-callback-handler.guard';

describe('OauthCallbackHandlerGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OauthCallbackHandlerGuard]
    });
  });

  it('should ...', inject([OauthCallbackHandlerGuard], (guard: OauthCallbackHandlerGuard) => {
    expect(guard).toBeTruthy();
  }));
});
