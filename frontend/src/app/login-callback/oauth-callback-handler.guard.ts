import { DataService } from './../shared/services/data.service';
import { User } from './../shared/classes/user';
import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AdalService } from './../shared/services/azure/adal.service';

@Injectable()

export class OAuthCallbackHandler implements CanActivate {

  constructor(private router: Router, private dataService: DataService, private adalService: AdalService) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    this.adalService.handleCallback();

    if (this.adalService.userInfo) {

      const returnUrl = route.queryParams['returnUrl'];

      if (!returnUrl) {

        this.router.navigate(['risk-matrix']);

      } else {

        this.router.navigate([returnUrl], { queryParams: route.queryParams });

      }

    } else {

      this.router.navigate(['login']);

    }

    return false;

  }

}
