import { AdalService } from './shared/services/azure/adal.service';
import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoaderService } from './shared/services/loader.service';
import { DataService } from './shared/services/data.service';
import { locale, loadMessages } from "devextreme/localization";

declare var UIkit: any;


let esMessages = {
    "es": {
        "Yes": "Sí",
        "No": "No",
        "Cancel": "Cancelar",
        "Clear": "Limpiar",
        "Done": "Hecho",
        "Loading": "Cargando...",
        "Select": "Seleccionar...",
        "Search": "Buscar",
        "Back": "Volver",
        "OK": "Aceptar",
        "dxCollectionWidget-noDataText": "Sin datos para mostrar",
        "validation-required": "Obligatorio",
        "validation-required-formatted": "{0} es obligatorio",
        "validation-numeric": "Valor debe ser un número",
        "validation-numeric-formatted": "{0} debe ser un número",
        "validation-range": "Valor fuera de rango",
        "validation-range-formatted": "{0} fuera de rango",
        "validation-stringLength": "El largo del valor es incorrecto",
        "validation-stringLength-formatted": "El largo de {0} es incorrecto",
        "validation-custom": "Valor inválido",
        "validation-custom-formatted": "{0} inválido",
        "validation-compare": "Valores no coinciden",
        "validation-compare-formatted": "{0} no coinciden",
        "validation-pattern": "Valor no coincide con el patrón",
        "validation-pattern-formatted": "{0} no coincide con el patrón",
        "validation-email": "Email inválido",
        "validation-email-formatted": "{0} inválido",
        "validation-mask": "Valor inválido",
        "dxLookup-searchPlaceholder": "Cantidad mínima de caracteres: {0}",
        "dxList-pullingDownText": "Desliza hacia abajo para actualizar...",
        "dxList-pulledDownText": "Suelta para actualizar...",
        "dxList-refreshingText": "Actualizando...",
        "dxList-pageLoadingText": "Cargando...",
        "dxList-nextButtonText": "Más",
        "dxList-selectAll": "Seleccionar Todo",
        "dxListEditDecorator-delete": "Eliminar",
        "dxListEditDecorator-more": "Más",
        "dxScrollView-pullingDownText": "Desliza hacia abajo para actualizar...",
        "dxScrollView-pulledDownText": "Suelta para actualizar...",
        "dxScrollView-refreshingText": "Actualizando...",
        "dxScrollView-reachBottomText": "Cargando...",
        "dxDateBox-simulatedDataPickerTitleTime": "Seleccione hora",
        "dxDateBox-simulatedDataPickerTitleDate": "Seleccione fecha",
        "dxDateBox-simulatedDataPickerTitleDateTime": "Seleccione fecha y hora",
        "dxDateBox-validation-datetime": "Valor debe ser una fecha u hora",
        "dxFileUploader-selectFile": "Seleccionar archivo",
        "dxFileUploader-dropFile": "o Arrastre un archivo aquí",
        "dxFileUploader-bytes": "bytes",
        "dxFileUploader-kb": "kb",
        "dxFileUploader-Mb": "Mb",
        "dxFileUploader-Gb": "Gb",
        "dxFileUploader-upload": "Subir",
        "dxFileUploader-uploaded": "Subido",
        "dxFileUploader-readyToUpload": "Listo para subir",
        "dxFileUploader-uploadFailedMessage": "Súbida fallo",
        "dxRangeSlider-ariaFrom": "Desde",
        "dxRangeSlider-ariaTill": "Hasta",
        "dxSwitch-onText": "ENCENDIDO",
        "dxSwitch-offText": "APAGADO",
        "dxForm-optionalMark": "opcional",
        "dxForm-requiredMessage": "{0} es obligatorio",
        "dxNumberBox-invalidValueMessage": "Valor debe ser un número",
        "dxDataGrid-columnChooserTitle": "Selector de Columnas",
        "dxDataGrid-columnChooserEmptyText": "Arrastra una columna aquí para esconderla",
        "dxDataGrid-groupContinuesMessage": "Continúa en la página siguiente",
        "dxDataGrid-groupContinuedMessage": "Continuación de la página anterior",
        "dxDataGrid-groupHeaderText": "Agrupar por esta columna",
        "dxDataGrid-ungroupHeaderText": "Desagrupar",
        "dxDataGrid-ungroupAllText": "Desagrupar Todo",
        "dxDataGrid-editingEditRow": "Editar",
        "dxDataGrid-editingSaveRowChanges": "Guardar",
        "dxDataGrid-editingCancelRowChanges": "Cancelar",
        "dxDataGrid-editingDeleteRow": "Eliminar",
        "dxDataGrid-editingUndeleteRow": "Recuperar",
        "dxDataGrid-editingConfirmDeleteMessage": "¿Está seguro que desea eliminar este registro?",
        "dxDataGrid-validationCancelChanges": "Cancelar cambios",
        "dxDataGrid-groupPanelEmptyText": "Arrastra una columna aquí para agrupar por ella",
        "dxDataGrid-noDataText": "Sin datos",
        "dxDataGrid-searchPanelPlaceholder": "Buscar...",
        "dxDataGrid-filterRowShowAllText": "(Todo)",
        "dxDataGrid-filterRowResetOperationText": "Reestablecer",
        "dxDataGrid-filterRowOperationEquals": "Igual",
        "dxDataGrid-filterRowOperationNotEquals": "No es igual",
        "dxDataGrid-filterRowOperationLess": "Menor que",
        "dxDataGrid-filterRowOperationLessOrEquals": "Menor que o igual a",
        "dxDataGrid-filterRowOperationGreater": "Mayor que",
        "dxDataGrid-filterRowOperationGreaterOrEquals": "Mayor que o igual a",
        "dxDataGrid-filterRowOperationStartsWith": "Empieza con",
        "dxDataGrid-filterRowOperationContains": "Contiene",
        "dxDataGrid-filterRowOperationNotContains": "No contiene",
        "dxDataGrid-filterRowOperationEndsWith": "Termina con",
        "dxDataGrid-filterRowOperationBetween": "Entre",
        "dxDataGrid-filterRowOperationBetweenStartText": "Inicio",
        "dxDataGrid-filterRowOperationBetweenEndText": "Fin",
        "dxDataGrid-applyFilterText": "Filtrar",
        "dxDataGrid-trueText": "verdadero",
        "dxDataGrid-falseText": "falso",
        "dxDataGrid-sortingAscendingText": "Orden Ascendente",
        "dxDataGrid-sortingDescendingText": "Orden Descendente",
        "dxDataGrid-sortingClearText": "Limpiar Ordenamiento",
        "dxDataGrid-editingSaveAllChanges": "Guardar cambios",
        "dxDataGrid-editingCancelAllChanges": "Descartar cambios",
        "dxDataGrid-editingAddRow": "Agregar una fila",
        "dxDataGrid-summaryMin": "Mín: {0}",
        "dxDataGrid-summaryMinOtherColumn": "Mín de {1} es {0}",
        "dxDataGrid-summaryMax": "Máx: {0}",
        "dxDataGrid-summaryMaxOtherColumn": "Máx de {1} es {0}",
        "dxDataGrid-summaryAvg": "Prom: {0}",
        "dxDataGrid-summaryAvgOtherColumn": "Prom de {1} es {0}",
        "dxDataGrid-summarySum": "Suma: {0}",
        "dxDataGrid-summarySumOtherColumn": "Suma de {1} es {0}",
        "dxDataGrid-summaryCount": "Recuento: {0}",
        "dxDataGrid-columnFixingFix": "Anclar",
        "dxDataGrid-columnFixingUnfix": "Desanclar",
        "dxDataGrid-columnFixingLeftPosition": "A la izquierda",
        "dxDataGrid-columnFixingRightPosition": "A la derecha",
        "dxDataGrid-exportTo": "Exportar",
        "dxDataGrid-exportToExcel": "Exportar a archivo Excel",
        "dxDataGrid-excelFormat": "Archivo Excel",
        "dxDataGrid-selectedRows": "Filas seleccionadas",
        "dxDataGrid-exportSelectedRows": "Exportar filas seleccionadas",
        "dxDataGrid-exportAll": "Exportar todo",
        "dxDataGrid-headerFilterEmptyValue": "(En Blanco)",
        "dxDataGrid-headerFilterOK": "Aceptar",
        "dxDataGrid-headerFilterCancel": "Cancelar",
        "dxDataGrid-ariaColumn": "Columna",
        "dxDataGrid-ariaValue": "Valor",
        "dxDataGrid-ariaFilterCell": "Celda de filtro",
        "dxDataGrid-ariaCollapse": "Colapsar",
        "dxDataGrid-ariaExpand": "Expandir",
        "dxDataGrid-ariaDataGrid": "Rejilla de datos",
        "dxDataGrid-ariaSearchInGrid": "Buscar en la rejilla de datos",
        "dxDataGrid-ariaSelectAll": "Seleccionar todo",
        "dxDataGrid-ariaSelectRow": "Seleccionar fila",
        "dxTreeList-ariaTreeList": "Lista de árbol",
        "dxTreeList-editingAddRowToNode": "Añadir",
        "dxPager-infoText": "Página {0} de {1} ({2} elementos)",
        "dxPager-pagesCountText": "de",
        "dxPivotGrid-grandTotal": "Gran Total",
        "dxPivotGrid-total": "{0} Total",
        "dxPivotGrid-fieldChooserTitle": "Selector de Campos",
        "dxPivotGrid-showFieldChooser": "Mostrar Selector de Campos",
        "dxPivotGrid-expandAll": "Expandir Todo",
        "dxPivotGrid-collapseAll": "Colapsar Todo",
        "dxPivotGrid-sortColumnBySummary": "Ordenar \"{0}\" por Esta Columna",
        "dxPivotGrid-sortRowBySummary": "Ordenar \"{0}\" por Esta Fila",
        "dxPivotGrid-removeAllSorting": "Remover Ordenamiento",
        "dxPivotGrid-dataNotAvailable": "N/A",
        "dxPivotGrid-rowFields": "Campos de Fila",
        "dxPivotGrid-columnFields": "Campos de Columna",
        "dxPivotGrid-dataFields": "Campos de Dato",
        "dxPivotGrid-filterFields": "Campos de Filtro",
        "dxPivotGrid-allFields": "Todos los Campos",
        "dxPivotGrid-columnFieldArea": "Arrastra Campos de Columna Aquí",
        "dxPivotGrid-dataFieldArea": "Arrastra Campos de Dato Aquí",
        "dxPivotGrid-rowFieldArea": "Arrastra Campos de Fila Aquí",
        "dxPivotGrid-filterFieldArea": "Arrastra Campos de Filtro Aquí",
        "dxScheduler-editorLabelTitle": "Asunto",
        "dxScheduler-editorLabelStartDate": "Fecha Inicio",
        "dxScheduler-editorLabelEndDate": "Fecha Término",
        "dxScheduler-editorLabelDescription": "Descripción",
        "dxScheduler-editorLabelRecurrence": "Repetir",
        "dxScheduler-openAppointment": "Abrir cita",
        "dxScheduler-recurrenceNever": "Nunca",
        "dxScheduler-recurrenceDaily": "Diario",
        "dxScheduler-recurrenceWeekly": "Semanal",
        "dxScheduler-recurrenceMonthly": "Mensual",
        "dxScheduler-recurrenceYearly": "Anual",
        "dxScheduler-recurrenceEvery": "Cada",
        "dxScheduler-recurrenceEnd": "Terminar repetición",
        "dxScheduler-recurrenceAfter": "Después",
        "dxScheduler-recurrenceOn": "En",
        "dxScheduler-recurrenceRepeatDaily": "día(s)",
        "dxScheduler-recurrenceRepeatWeekly": "semana(s)",
        "dxScheduler-recurrenceRepeatMonthly": "mes(es)",
        "dxScheduler-recurrenceRepeatYearly": "año(s)",
        "dxScheduler-switcherDay": "Día",
        "dxScheduler-switcherWeek": "Semana",
        "dxScheduler-switcherWorkWeek": "Semana Laboral",
        "dxScheduler-switcherMonth": "Mes",
        "dxScheduler-switcherAgenda": "Agenda",
        "dxScheduler-switcherTimelineDay": "Línea de tiempo Día",
        "dxScheduler-switcherTimelineWeek": "Línea de tiempo Semana",
        "dxScheduler-switcherTimelineWorkWeek": "Línea de tiempo Semana Laboral",
        "dxScheduler-switcherTimelineMonth": "Línea de tiempo Mes",
        "dxScheduler-recurrenceRepeatOnDate": "en la fecha",
        "dxScheduler-recurrenceRepeatCount": "ocurrencia(s)",
        "dxScheduler-allDay": "Todo el día",
        "dxScheduler-confirmRecurrenceEditMessage": "¿Quiere editar solo esta cita o toda la serie?",
        "dxScheduler-confirmRecurrenceDeleteMessage": "¿Quiere eliminar solo esta cita o toda la serie?",
        "dxScheduler-confirmRecurrenceEditSeries": "Editar serie",
        "dxScheduler-confirmRecurrenceDeleteSeries": "Eliminar serie",
        "dxScheduler-confirmRecurrenceEditOccurrence": "Editar cita",
        "dxScheduler-confirmRecurrenceDeleteOccurrence": "Eliminar cita",
        "dxScheduler-noTimezoneTitle": "Sin zona horaria",
        "dxScheduler-moreAppointments": "{0} más",
        "dxCalendar-todayButtonText": "Hoy",
        "dxCalendar-ariaWidgetName": "Calendario",
        "dxColorView-ariaRed": "Rojo",
        "dxColorView-ariaGreen": "Verde",
        "dxColorView-ariaBlue": "Azul",
        "dxColorView-ariaAlpha": "Transparencia",
        "dxColorView-ariaHex": "Código de color",
        "dxTagBox-selected": "{0} seleccionado",
        "dxTagBox-allSelected": "Todos seleccionados ({0})",
        "dxTagBox-moreSelected": "{0} más",
        "vizExport-printingButtonText": "Imprimir",
        "vizExport-titleMenuText": "Exportar/Imprimir",
        "vizExport-exportButtonText": "Archivo {0}",
        "dxFilterBuilder-and": "E",
        "dxFilterBuilder-or": "O",
        "dxFilterBuilder-notAnd": "NO E",
        "dxFilterBuilder-notOr": "NO O",
        "dxFilterBuilder-addCondition": "Añadir condición",
        "dxFilterBuilder-addGroup": "Añadir Grupo",
        "dxFilterBuilder-enterValueText": "<rellene con un valor>",
        "dxFilterBuilder-filterOperationEquals": "Igual",
        "dxFilterBuilder-filterOperationNotEquals": "Diferente",
        "dxFilterBuilder-filterOperationLess": "Menos que",
        "dxFilterBuilder-filterOperationLessOrEquals": "Menor o igual que",
        "dxFilterBuilder-filterOperationGreater": "Más grande que",
        "dxFilterBuilder-filterOperationGreaterOrEquals": "Mayor o igual que",
        "dxFilterBuilder-filterOperationStartsWith": "Comienza con",
        "dxFilterBuilder-filterOperationContains": "Contiene",
        "dxFilterBuilder-filterOperationNotContains": "No contiene",
        "dxFilterBuilder-filterOperationEndsWith": "Termina con",
        "dxFilterBuilder-filterOperationIsBlank": "Vacio",
        "dxFilterBuilder-filterOperationIsNotBlank": "No vacio"
    }
}


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
    ultimoModulo = 'risk-matrix';
    relatedModules = 'risk-matrix,fmeca,rcm,maintenance-plan';
    rmesModules: any = [
        {
            id: 0,
            key: 'risk-matrix',
            url: 'risk-matrix',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'Risk Matrix',
            keywords: 'risk,matrix,risk matrix,riesgo,matriz,maintenance,mantenimiento',
            enabled: true,
            filtered_out: false,
            disponible: true
        },
        {
            id: 0,
            key: 'fmeca',
            url: 'fmeca',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'FMECA',
            keywords: 'fmeca,maintenance,mantenimiento',
            enabled: true,
            filtered_out: false,
            disponible: true
        },
        {
            id: 0,
            key: 'rcm',
            url: 'rcm',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'RCM',
            keywords: 'rcm,maintenance,mantenimiento',
            enabled: true,
            filtered_out: false,
            disponible: true
        },
        {
            id: 0,
            key: 'maintenance-plan',
            url: 'maintenance-plan',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'Plan de Mtto.',
            keywords: 'plan,mantenimiento,plan de mantenimiento,plan mantenimiento',
            enabled: true,
            filtered_out: false,
            disponible: false
        },
        {
            id: 0,
            key: '',
            url: '#',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'Data Fill',
            keywords: 'Data Fill',
            enabled: false,
            filtered_out: false,
            disponible: false
        },
        {
            id: 0,
            key: '',
            url: '#',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'Custom Reports',
            keywords: 'Custom Reports',
            enabled: false,
            filtered_out: false,
            disponible: false
        },
        {
            id: 0,
            key: '',
            url: '#',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'RCA',
            keywords: 'RCA, root cause analysis',
            enabled: true,
            filtered_out: false,
            disponible: false
        },
        {
            id: 0,
            key: '',
            url: '#',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'Dashboard',
            keywords: 'Dashboard',
            enabled: true,
            filtered_out: false,
            disponible: false
        },
        {
            id: 0,
            key: '',
            url: '#',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'Total Assets',
            keywords: 'Total Assets',
            enabled: true,
            filtered_out: false,
            disponible: false
        },
        {
            id: 0,
            key: '',
            url: '#',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'RAM Analysis',
            keywords: 'RAM Analysis',
            enabled: true,
            filtered_out: false,
            disponible: false
        },
        {
            id: 0,
            key: '',
            url: '#',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'Cluster',
            keywords: 'Cluster',
            enabled: false,
            filtered_out: false,
            disponible: false
        },
        {
            id: 0,
            key: '',
            url: '#',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'OEE',
            keywords: 'OEE, overall equipment effectiveness',
            enabled: true,
            filtered_out: false,
            disponible: false
        },
        {
            id: 0,
            key: '',
            url: '#',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'Data Mining',
            keywords: 'Data Mining',
            enabled: true,
            filtered_out: false,
            disponible: false
        },
        {
            id: 0,
            key: '',
            url: '#',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'RBM',
            keywords: 'RBM',
            enabled: false,
            filtered_out: false,
            disponible: false
        },
        {
            id: 0,
            key: '',
            url: '#',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'SPRISK',
            keywords: 'SPRISK',
            enabled: true,
            filtered_out: false,
            disponible: false
        },
        {
            id: 0,
            key: '',
            url: '#',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'Maint. Services',
            keywords: 'Maint. Services,maintenance',
            enabled: true,
            filtered_out: false,
            disponible: false
        },
        {
            id: 0,
            key: 'hazop',
            url: 'hazop',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'HAZOP',
            keywords: 'hazid,hazop',
            enabled: true,
            filtered_out: false,
            disponible: true   
        },
        {
            id: 0,
            key: 'hazid',
            url: 'hazid',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'HAZID',
            keywords: 'hazid,hazop',
            enabled: true,
            filtered_out: false,
            disponible: true
        },
        {
            id: 0,
            key: '',
            url: '#',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'Plan Forecast',
            keywords: 'Plan Forecast',
            enabled: false,
            filtered_out: false,
            disponible: false
        },
        {
            id: 0,
            key: '',
            url: '#',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'Plan Sim',
            keywords: 'Plan Sim, simulador, simulator',
            enabled: true,
            filtered_out: false,
            disponible: false
        },
        {
            id: 0,
            key: '',
            url: '#',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'LCC',
            keywords: 'LCC, life cycle cost',
            enabled: true,
            filtered_out: false,
            disponible: false
        },
        {
            id: 0,
            key: '',
            url: '#',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'POS',
            keywords: 'POS',
            enabled: true,
            filtered_out: false,
            disponible: false
        },
        {
            id: 0,
            key: '',
            url: '#',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'RBD Manager',
            keywords: 'RBD Manager',
            enabled: true,
            filtered_out: false,
            disponible: false
        },
        {
            id: 0,
            key: 'settings',
            url: 'settings',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'Settings',
            keywords: 'Settings',
            enabled: true,
            filtered_out: false,
            disponible: true
        },
        {
            id: 0,
            key: '',
            url: '#',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'Docs',
            keywords: 'Docs',
            enabled: true,
            filtered_out: false,
            disponible: false
        },
        {
            id: 0,
            key: '',
            url: '#',
            icon_url: './assets/img/icons/pie-chart.png',
            name: 'Support',
            keywords: 'Support, soporte',
            enabled: true,
            filtered_out: true,
            disponible: false
        }
    ];
    modulesFilterTerm = '';
    isLoading = false;
    _subscription;

    constructor(private router: Router,
        public dataService: DataService,
        public loaderService: LoaderService,
        private cdRef: ChangeDetectorRef,
        private adalService: AdalService) {
			    loadMessages(esMessages);
				locale(navigator.language);

    }

    ngOnInit() {
        // Si se recarga la pagina de la aplicacion, recuperar sesion
        const req = this.dataService.recoverSession();
        this.cdRef.detectChanges();
    }

    ngAfterViewInit() {
        this._subscription = this.loaderService.loadingChange.subscribe((value) => {
            this.isLoading = value;
        });
        this.cdRef.detectChanges();
    }

    ngOnDestroy() {
        // Se previenen fugas de memoria
        this._subscription.unsubscribe();
    }

    navigateToModule(module) {

        if (module.disponible == true) {
            this.ultimoModulo = module.url;
            this.router.navigateByUrl('/' + module.url);
        }
        else{
            UIkit.notification({
                message: 'Este modulo no se encuentra disponible',
                status: 'warning',
                pos: 'top-center',
                timeout: 4000
              });
            this.router.navigateByUrl('/' + this.ultimoModulo);
        }
    }

    filterModules(): void {
        this.modulesFilterTerm = this.modulesFilterTerm.trim().toLowerCase();
        for (let i = 0; i < this.rmesModules.length; i++) {
            const module = this.rmesModules[i];

            if (module.enabled === false ||
                (this.modulesFilterTerm !== '' && module.keywords.toLowerCase().indexOf(this.modulesFilterTerm) === -1)) {
                module.filtered_out = true;
            } else {
                module.filtered_out = false;
            }

        }
    }
}
