'use strict';

export const Globals = Object.freeze({
    COMPANY_NAME: 'CGS S.A.',
    COMPANY_CONTACT: 'Alessio Arata <alessio.arata@cgssa.com>',

    APP_VERSION: '1.0',
    APP_LOCAL_STORAGE_KEY: 'cgs_rmes',

    // API_BASE_URL: 'http://suitedev.rmessuite.com/maintenance-strategy-backend/' // Testing @ suitedev.cgssa.com
    API_BASE_URL: 'http://localhost/cursos/hzd/' // Localhost
  //  API_BASE_URL: 'http://vps234941.vps.ovh.ca/hazop-hazid/' // Localhost
});
