<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hazop_hazid_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function get_parts($subsystem_id)
    {
        $this->db->select('co as id, co_subsistem as subsystem_id, nb as name, co_tipo as type_id, vers as version, fe_act as date_modified, std as status')
            ->from('tsgen_subsistems_parts')
            ->where('co_subsistem', $subsystem_id)
            ->where('std', 1);

        $query = $this->db->get();

        if ($query) {
            return $query->result();
        }
        return [];
    }

    public function get_part_hazop($part_id)
    {
        $this->db->select('co as id, co_part as part_id, co_categ as category_id, co_plbr_guia as guide_word_id, co_param as parameter_id, causa as cause, ctrl_actl as current_control, efc as effect, frec as frequency, consec_seg as consequence_security, consec_ambt as consequence_environment, consec_comunid as consequence_community, consec_rept as consequence_reputation, consec_legal as consequence_legal, consec_finc as consequence_finance, riesgo as risk, co_acc as action_id, respb as responsible, fe_reg as date_created, fe_act as date_modified, std as status, detc as detection')
            ->from('tshzd_hzp')
            ->where('std', 1)
            ->where('co_part', $part_id);

        $query = $this->db->get();

        if ($query) {
            $result = $query->result();
            $corrected_result = [];

            foreach ($result as $key => $row) {
                // Reemplazamos keys_con_guiones por keys-con-guiones para el frontend
                $corrected_row = []; //
                foreach ($row as $key => $value) {
                    $corrected_row[str_replace('_', '-', $key)] = $value;
                }
                $corrected_result[] = $corrected_row;
            }
            return $corrected_result;
        }

        return [];
    }

    public function get_part_hazid($part_id)
    {
        $this->db->select('co as id, co_part as part_id, co_categ as category_id, co_plbr_guia as guide_word_id, causa as cause, ctrl_actl as current_control, efc as effect, frec as frequency, consec_seg as consequence_security, consec_ambt as consequence_environment, consec_comunid as consequence_community, consec_rept as consequence_reputation, consec_legal as consequence_legal, consec_finc as consequence_finance, riesgo as risk, acc as action, respb as responsible, fe_reg as date_created, fe_act as date_modified, std as status, detc as detection')
            ->from('tshzd_hzd')
            ->where('std', 1)
            ->where('co_part', $part_id);

        $query = $this->db->get();

        if ($query) {
            $result = $query->result();
            $corrected_result = [];

            foreach ($result as $key => $row) {
                // Reemplazamos keys_con_guiones por keys-con-guiones para el frontend
                $corrected_row = []; //
                foreach ($row as $key => $value) {
                    $corrected_row[str_replace('_', '-', $key)] = $value;
                }
                $corrected_result[] = $corrected_row;
            }
            return $corrected_result;
        }

        return [];
    }

    public function delete_parts($node)
    {
        // Se separa la Parte de sus FMECA-RCM y se almacenan
        foreach ($node->parts as $part) {

            $hazop = $part->hazop;
            $hazid = $part->hazid;
            $part->date_modified = $part->date_modified ? $part->date_modified : date("Y-m-d H:i:s");
            unset($part->fmeca);
            unset($part->hazop);
            unset($part->hazid);

            // Si no trae tipo, se setea en 0
            if (!$part->type) {
                $part->type_id = 0;
            }
            unset($part->type);

            $dic_part = array('id' => 'co', 'subsystem_id' => 'co_subsistem', 'name' => 'nb', 'type_id' => 'co_tipo',
                            'version' => 'vers', 'date_modified' => 'fe_act', 'status' => 'std');
            $part_row = [];

            foreach ($part as $key => $value) {

                foreach ($dic_part as $clave => $valor) {

                    if ($key == $clave) {

                        $part_row[$valor] = $value;
                    }
                }
            }

            $part_id = $part->id ? $part->id : $this->db->insert_id();

            if($part_row['std'] == 0){
                $this->db->where('co', $part_row['co']);
                $this->db->delete('tsgen_subsistems_parts');
            }
            if ($hazid != null) {
                $this->deleteHzd($part_id, $hazid, $part_row['std']);
            }
            if ($hazop != null) {
                $this->deleteHzp($part_id, $hazop, $part_row['std']);
            }
        }
    }

    public function deleteHzp($part_id, $hazop, $std)
    {
        foreach ($hazop->data as $row) {

            $dic_hzp = array('co_part' => 'co_part', 'id' => 'co', 'category_id' => 'co_categ', 'guide_word_id' => 'co_plbr_guia', 'parameter_id' => 'co_param', 'cause' => 'causa',
                            'current_control' => 'ctrl_actl', 'effect' => 'efc', 'frequency' => 'frec', 'consequence_security' => 'consec_seg', 'consequence_environment' => 'consec_ambt',
                            'consequence_community' => 'consec_comunid', 'consequence_reputation' => 'consec_rept' , 'consequence_legal' => 'consec_legal', 'consequence_finance' => 'consec_finc',
                            'risk' => 'riesgo', 'action_id' => 'co_acc', 'responsible' => 'respb', 'date_created' => 'fe_reg', 'date_modified' => 'fe_act', 'status' => 'std');

            unset($row->{'part-id'});
            // Reemplazamos keys-con-guiones por keys_con_guiones para la db
            $corrected_row = [];
            $corrected_row['co_part'] = $part_id;

            foreach ($row as $key => $value) {

                $corrected_row[str_replace('-', '_', $key)] = $value;
            }

            $hzp_row = [];

            foreach ($corrected_row as $key => $value) {

                foreach ($dic_hzp as $clave => $valor) {

                    if ($key == $clave) {

                        $hzp_row[$valor] = $value;
                    }
                }
            }

            if($std == 0){
                $hzp_row['std'] = 0;
            }

            if($hzp_row['std'] == 0){
                $this->db->where('co', $hzp_row['co']);
                $this->db->delete('tshzd_hzp');
            }

        }
    }

    public function deleteHzd($part_id, $hazid, $std)
    {
        foreach ($hazid->data as $row) {

            $dic_hzp = array('co_part' => 'co_part', 'id' => 'co', 'category_id' => 'co_categ', 'guide_word_id' => 'co_plbr_guia', 'parameter_id' => 'co_param', 'cause' => 'causa',
                            'current_control' => 'ctrl_actl', 'effect' => 'efc', 'frequency' => 'frec', 'consequence_security' => 'consec_seg', 'consequence_environment' => 'consec_ambt',
                            'consequence_community' => 'consec_comunid', 'consequence_reputation' => 'consec_rept' , 'consequence_legal' => 'consec_legal', 'consequence_finance' => 'consec_finc',
                            'risk' => 'riesgo', 'action_id' => 'co_acc', 'responsible' => 'respb', 'date_created' => 'fe_reg', 'date_modified' => 'fe_act', 'status' => 'std');

            unset($row->{'part-id'});
            // Reemplazamos keys-con-guiones por keys_con_guiones para la db
            $corrected_row = [];
            $corrected_row['co_part'] = $part_id;

            foreach ($row as $key => $value) {

                $corrected_row[str_replace('-', '_', $key)] = $value;
            }

            $hzd_row = [];

            foreach ($corrected_row as $key => $value) {

                foreach ($dic_hzp as $clave => $valor) {

                    if ($key == $clave) {

                        $hzd_row[$valor] = $value;
                    }
                }
            }

            if($std == 0){
                $hzd_row['std'] = 0;
            }

            if($hzd_row['std'] == 0){
                $this->db->where('co', $hzd_row['co']);
                $this->db->delete('tshzd_hzd');
            }

        }
    }

    public function update_parts_hazop($node)
    {
        // Se separa la Parte de sus FMECA-RCM y se almacenan
        foreach ($node->parts as $part) {

            $hazop = $part->hazop;
            $part->date_modified = $part->date_modified ? $part->date_modified : date("Y-m-d H:i:s");
            unset($part->fmeca);
            unset($part->hazop);
            unset($part->hazid);
            unset($part->type);

            $dic_part = array('id' => 'co', 'subsystem_id' => 'co_subsistem', 'name' => 'nb', 'type_id' => 'co_tipo',
                            'version' => 'vers', 'date_modified' => 'fe_act', 'status' => 'std');
            $part_row = [];
            $part_row_update = [];
            $part_row_insert = [];

            foreach ($part as $key => $value) {

                foreach ($dic_part as $clave => $valor) {

                    if ($key == $clave) {

                        $part_row[$valor] = $value;
                    }
                }
            }

            if ($part_row['co'] == NULL) {
                
                $part_row_insert = $part_row;
                unset($part_row_insert['co']);

                $this->db->insert('tsgen_subsistems_parts', $part_row_insert);
                
            }

            else {
                $part_row_update = $part_row;
                $index = $part_row_update['co'];
                unset($part_row_update['co']);

                $this->db->set($part_row_update, 0);
                $this->db->where('co', $index);
                $this->db->update('tsgen_subsistems_parts');
            }

            //$this->db->replace('tsgen_subsistems_parts', $part_row);

            $part_id = $part->id ? $part->id : $this->db->insert_id();
            // Ahora se guarda hazop
            $this->update_hazop($part_id, $hazop);
        }
    }

    public function update_hazop($part_id, $hazop_hazid)
    {
        foreach ($hazop_hazid->data as $row) {

            $dic_hzp = array('co_part' => 'co_part', 'id' => 'co', 'category_id' => 'co_categ', 'guide_word_id' => 'co_plbr_guia', 'parameter_id' => 'co_param', 'cause' => 'causa',
                            'current_control' => 'ctrl_actl', 'effect' => 'efc', 'frequency' => 'frec', 'consequence_security' => 'consec_seg', 'consequence_environment' => 'consec_ambt',
                            'consequence_community' => 'consec_comunid', 'consequence_reputation' => 'consec_rept' , 'consequence_legal' => 'consec_legal', 'consequence_finance' => 'consec_finc',
                            'risk' => 'riesgo', 'action_id' => 'co_acc', 'responsible' => 'respb', 'date_created' => 'fe_reg', 'date_modified' => 'fe_act', 'status' => 'std ', 'detection' => 'detc');

            unset($row->{'part-id'});
            // Reemplazamos keys-con-guiones por keys_con_guiones para la db
            $corrected_row = [];
            $corrected_row['co_part'] = $part_id;

            foreach ($row as $key => $value) {

                $corrected_row[str_replace('-', '_', $key)] = $value;
            }

            $hzp_row = [];
            $hzp_row_insert = [];
            $hzp_row_update = [];

            foreach ($corrected_row as $key => $value) {

                foreach ($dic_hzp as $clave => $valor) {

                    if ($key == $clave) {

                        $hzp_row[$valor] = $value;
                    }
                }
            }

            if ($hzp_row['co'] == NULL) {
                
                $hzp_row_insert = $hzp_row;
                unset($hzp_row_insert['co']);

                $this->db->insert('tshzd_hzp', $hzp_row_insert);
                
            }

            else {
                $hzp_row_update = $hzp_row;
                $index = $hzp_row_update['co'];
                unset($hzp_row_update['co']);

                $this->db->set($hzp_row_update, 0);
                $this->db->where('co', $index);
                $this->db->update('tshzd_hzp');
            }

            //$this->db->replace('tshzd_hzp', $hzp_row);
        }
    }

    public function update_parts_hazid($node)
    {

        // Se separa la Parte de sus FMECA-RCM y se almacenan
        foreach ($node->parts as $part) {

            $hazid = $part->hazid;
            $part->date_modified = $part->date_modified ? $part->date_modified : date("Y-m-d H:i:s");
            unset($part->fmeca);
            unset($part->hazop);
            unset($part->hazid);
            unset($part->type);

            $dic_part = array('id' => 'co', 'subsystem_id' => 'co_subsistem', 'name' => 'nb', 'type_id' => 'co_tipo',
                            'version' => 'vers', 'date_modified' => 'fe_act', 'status' => 'std');
            $part_row = [];
            $part_row_insert = [];
            $part_row_update = [];

            foreach ($part as $key => $value) {

                foreach ($dic_part as $clave => $valor) {

                    if ($key == $clave) {

                        $part_row[$valor] = $value;
                    }
                }
            }
                                    
            if ($part_row['co'] == NULL) {
                
                $part_row_insert = $part_row;
                unset($part_row_insert['co']);

                $this->db->insert('tsgen_subsistems_parts', $part_row_insert);
                
            }

            else {
                $part_row_update = $part_row;
                $index = $part_row_update['co'];
                unset($part_row_update['co']);

                $this->db->set($part_row_update, 0);
                $this->db->where('co', $index);
                $this->db->update('tsgen_subsistems_parts');
            }

            //$this->db->replace('tsgen_subsistems_parts', $part_row);

            $part_id = $part->id ? $part->id : $this->db->insert_id();
            // Ahora se guarda hazid
            $this->update_hazid($part_id, $hazid);
        }
    }

    public function update_hazid($part_id, $hazid)
    {
       
        foreach ($hazid->data as $row) {
            
            $dic_hzd = array('co_part' => 'co_part', 'id' => 'co','category_id' => 'co_categ', 'guide_word_id' => 'co_plbr_guia', 'cause' => 'causa', 'current_control' => 'ctrl_actl', 'effect' => 'efc', 'frequency' => 'frec',
                         'consequence_security' => 'consec_seg', 'consequence_environment' => 'consec_ambt', 'consequence_community' => 'consec_comunid', 'consequence_reputation' => 'consec_rept' , 'consequence_legal' => 'consec_legal',
                         'consequence_finance' => 'consec_finc', 'risk' => 'riesgo', 'action' => 'acc', 'responsible' => 'respb', 'date_created' => 'fe_reg' ,'date_modified' => 'fe_act','status' => 'std', 'detection' => 'detc');

            unset($row->{'part-id'});
            // Reemplazamos keys-con-guiones por keys_con_guiones para la db
            $corrected_row = [];
            $corrected_row['co_part'] = $part_id;

            foreach ($row as $key => $value) {

                $corrected_row[str_replace('-', '_', $key)] = $value;
            }
            
            $hzd_row = [];
            $hzd_row_insert = [];
            $hzd_row_update = [];

            foreach ($corrected_row as $key => $value) {

                foreach ($dic_hzd as $clave => $valor) {

                    if ($key == $clave) {

                        $hzd_row[$valor] = $value;
                    }
                }
            }
                                                
            if ($hzd_row['co'] == NULL) {
                
                $hzd_row_insert = $hzd_row;
                unset($hzd_row_insert['co']);

                $this->db->insert('tshzd_hzd', $hzd_row_insert);
                
            }

            else {
                $hzd_row_update = $hzd_row;
                $index = $hzd_row_update['co'];
                unset($hzd_row_update['co']);

                $this->db->set($hzd_row_update, 0);
                $this->db->where('co', $index);
                $this->db->update('tshzd_hzd');
            }

            //$this->db->replace('tshzd_hzd', $hzd_row);
        }
    }

    public function get_part_types()
    {

        $this->db->select('co as id, nb as name, std as status')
            ->from('tsgen_subsistems_parts_tipos')
            ->where('std', 1);

        $query = $this->db->get();

        return $query->result();
    }

    public function get_part_type_parameters($part_type_id)
    {

        $this->db->select('co as id, co_part_tipo as part_type_id, nb as name, std as status')
            ->from('tshzd_subsistems_parts_tipos_params')
            ->where('co_part_tipo', $part_type_id)
            ->where('std', 1);

        $query = $this->db->get();

        return $query->result();
    }

    public function get_part_type_actions($part_type_id)
    {

        $this->db->select('co as id, co_part_tipo as part_type_id, nb as name, std as status')
            ->from('tshzd_subsistems_parts_tipos_acc')
            ->where('co_part_tipo', $part_type_id)
            ->where('std', 1);

        $query = $this->db->get();

        return $query->result();
    }

    public function update_part_types($part_types)
    {

        foreach ($part_types as $pt) {

            if ($pt->id) {

                $this->db->where('co', $pt->id);
                $this->db->update('tsgen_subsistems_parts_tipos', [
                    'nb' => $pt->name,
                    'std' => $pt->status ? 1 : 0,
                ]);

                if ($pt->status == 0) {
                    $this->clear_parts_type($pt->id);
                }

            } else {

                if ($pt->status === 1) {
                    $this->db->insert('tsgen_subsistems_parts_tipos', [
                        'nb' => $pt->name,
                        'std' => $pt->status ? 1 : 0,
                    ]);

                    $insert_id = $this->db->insert_id();

                    if ($insert_id) {
                        $pt->id = $insert_id;
                    }
                }

            }

            $this->update_part_types_parameters($pt->id, $pt->parameters);
            $this->update_part_types_actions($pt->id, $pt->actions);

        }

    }

    private function update_part_types_parameters($part_id, $parameters)
    {

        foreach ($parameters as $param) {

            if ($param->id) {

                $this->db->where('co', $param->id);
                $this->db->update('tshzd_subsistems_parts_tipos_params', [
                    'nb' => $param->name,
                    'co_part_tipo' => $part_id,
                    'std' => $param->status ? 1 : 0,
                ]);

                if ($param->status === 0) {
                    $this->clear_parts_type_params($param->id);
                }

            } else {

                if ($param->status === 1) {
                    $this->db->insert('tshzd_subsistems_parts_tipos_params', [
                        'nb' => $param->name,
                        'co_part_tipo' => $part_id,
                        'std' => $param->status ? 1 : 0,
                    ]);
                }

            }

        }

    }

    private function update_part_types_actions($part_id, $actions)
    {

        foreach ($actions as $action) {

            if ($action->id) {

                $this->db->where('co', $action->id);
                $this->db->update('tshzd_subsistems_parts_tipos_acc', [
                    'nb' => $action->name,
                    'co_part_tipo' => $part_id,
                    'std' => $action->status ? 1 : 0,
                ]);

                if ($action->status === 0) {
                    $this->clear_parts_type_actions($action->id);
                }

            } else {

                if ($action->status === 1) {
                    $this->db->insert('tshzd_subsistems_parts_tipos_acc', [
                        'nb' => $action->name,
                        'co_part_tipo' => $part_id,
                        'std' => $action->status ? 1 : 0,
                    ]);
                }

            }

        }

    }

    public function get_guide_words()
    {
        $this->db->select('co as id, plbr_guia as guide_word, ds as description, std as status')
            ->from('tshzd_hzp_plbr_guia')
            ->where('std', 1);

        $query = $this->db->get();

        if ($query) {
            return $query->result();
        }

        return [];
    }

    public function update_guide_words($guide_words)
    {

        foreach ($guide_words as $row) {

            // Para filas existentes
            if ($row->id) {

                $this->db->where('co', $row->id);
                $this->db->update('tshzd_hzp_plbr_guia', [
                    'plbr_guia' => $row->guide_word,
                    'ds' => $row->description,
                    'std' => $row->status ? 1 : 0,
                ]);

                if ($row->status === 0) {
                    // Se actualizan los elementos dependiente de esta palabra guia
                    $this->clear_parts_guide_word($row->id);
                }

            } else { // Nueva palabra guia

                if ($row->status) {
                    $this->db->insert('tshzd_hzp_plbr_guia', [
                        'plbr_guia' => $row->guide_word,
                        'ds' => $row->description,
                        'std' => 1,
                    ]);
                }
            }
        }

    }

    public function get_hazid_categories()
    {

        $this->db->select('co as id, nb as name, std as status')
            ->from('tshzd_hzd_categ')
            ->where('std', 1);

        $query = $this->db->get();

        if ($query) {
            return $query->result();
        }

        return [];
    }

    public function get_category_guide_words($category_id)
    {

        $this->db->select('co as id, co_catg as category_id, plbr_guia as guide_word, std as status')
            ->from('tshzd_hzd_categ_plbr_guia')
            ->where('co_catg', $category_id);

        $query = $this->db->get();

        return $query->result();
    }

    public function update_hazid_categories($categories)
    {

        foreach ($categories as $cat) {

            if ($cat->id) {

                $this->db->where('co', $cat->id);
                $this->db->update('tshzd_hzd_categ', [
                    'nb' => $cat->name,
                    'std' => $cat->status ? 1 : 0,
                ]);

                if ($cat->status === 0) {
                    $this->clear_parts_hazid_categories($cat->id);
                }

            } else {

                if ($cat->status === 1) {
                    $this->db->insert('tshzd_hzd_categ', [
                        'nb' => $cat->name,
                        'std' => $cat->status ? 1 : 0,
                    ]);

                    $insert_id = $this->db->insert_id();

                    if ($insert_id) {
                        $cat->id = $insert_id;
                    }
                }

            }

            $this->update_hazid_categories_guide_words($cat->id, $cat->guide_words);

        }

    }

    private function update_hazid_categories_guide_words($category_id, $guide_words)
    {

        foreach ($guide_words as $guide_word) {

            if ($guide_word->id) {

                $this->db->where('co', $guide_word->id);
                $this->db->update('tshzd_hzd_categ_plbr_guia', [
                    'plbr_guia' => $guide_word->guide_word,
                    'co_catg' => $category_id,
                    'std' => $guide_word->status ? 1 : 0,
                ]);

                if ($guide_word->status === 0) {
                    $this->clear_parts_hazid_guide_words($guide_word->id);
                }

            } else {

                if ($guide_word->status === 1) {
                    $this->db->insert('tshzd_hzd_categ_plbr_guia', [
                        'plbr_guia' => $guide_word->guide_word,
                        'co_catg' => $category_id,
                        'std' => $guide_word->status ? 1 : 0,
                    ]);
                }

            }

        }

    }

    private function clear_parts_guide_word($guide_word_id)
    {
        if (!$guide_word_id) {
            return false;
        }

        $this->db->set('co_plbr_guia', 0);
        $this->db->where('co_plbr_guia', $guide_word_id);
        $this->db->update('tshzd_hzp');

        return true;
    }

    private function clear_parts_type($part_type_id)
    {
        if (!$part_type_id) {
            return false;
        }

        $this->db->set('co_tipo', 0);
        $this->db->where('co_tipo', $part_type_id);
        $this->db->update('tsgen_subsistems_parts');

        return true;
    }

    private function clear_parts_type_params($param_id)
    {
        if (!$param_id) {
            return false;
        }

        $this->db->set('co_param', 0);
        $this->db->where('co_param', $param_id);
        $this->db->update('tshzd_hzp');

        return true;
    }

    private function clear_parts_type_actions($action_id)
    {
        if (!$action_id) {
            return false;
        }

        $this->db->set('co_acc', 0);
        $this->db->where('co_acc', $action_id);
        $this->db->update('tshzd_hzp');

        return true;
    }

    private function clear_parts_hazid_categories($category_id)
    {
        if (!$category_id) {
            return false;
        }

        $this->db->set('co_categ', 0);
        $this->db->where('co_categ', $category_id);
        $this->db->update('tshzd_hzd');

        return true;
    }

    private function clear_parts_hazid_guide_words($guide_word_id)
    {
        if (!$guide_word_id) {
            return false;
        }

        $this->db->set('co_plbr_guia', 0);
        $this->db->where('co_plbr_guia', $guide_word_id);
        $this->db->update('tshzd_hzd');

        return true;
    }

    public function get_riskMatrix_used()
    {

        $resultFrec = $this->select_('frequency', 'frec');

        $resultCS = $this->select_('consequence_security', 'consec_seg');
        $resultCE = $this->select_('consequence_environment',  'consec_ambt');
        $resultCC = $this->select_('consequence_community',  'consec_comunid');
        $resultCR = $this->select_('consequence_reputation', 'consec_rept');
        $resultCL = $this->select_('consequence_legal', 'consec_legal');
        $resultCF = $this->select_('consequence_finance', 'consec_finc');
        $resultC =[$resultCS, $resultCE, $resultCC, $resultCR, $resultCL, $resultCF];
        $resultConsec = []; 

        foreach ($resultC as $k => $v) {
            foreach ($v as $key => $value) {
                array_push($resultConsec, $value);
            }
        }

        $resultConsec = array_unique($resultConsec);

        $resultDetc = $this->select_('detection', 'detc');

        $result['frequency'] = $resultFrec;
        $result['consequence'] = $resultConsec;
        $result['detection'] = $resultDetc;

        return $result;

    }

    public function select_($as, $group_by )
    {
        $string = $group_by.' as '.$as;
        $this->db->select($string)
            ->from('tshzd_hzd')
            ->where('std', 1)
            ->group_by($group_by);

        $query1 = $this->db->get();
        $result1 = $query1->result();

        $this->db->select($string)
            ->from('tshzd_hzp')
            ->where('std', 1)
            ->group_by($group_by);

        $query2 = $this->db->get();
        $result2 = $query2->result();

        $result[0] = $result1;
        $result[1] = $result2;
        $resultado = [];

        foreach ($result[1] as $k => $i) {
            foreach ($i as $key => $value) {
                array_push($resultado, $value);
            }
        }

        foreach ($result[0] as $k => $i) {
            foreach ($i as $key => $value) {
                array_push($resultado, $value);
            }
        }

        foreach ($resultado as $key => $value) {
            if (empty($value)) {
                $resultado[$key] = 0;
            }
        }

        $resultado = array_unique($resultado); 

        return $resultado;
    }
}