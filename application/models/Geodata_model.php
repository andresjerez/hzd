<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Geodata_Model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function load_geodata_by_plant($id_planta = null)
    {

        $this->db->select('g.id_geo, g.lat, g.lng, g.geocoder, g.elevation');
        $this->db->from('geodata as g');
        $this->db->join('plantas as p', 'p.id_geo = g.id_geo');
        $this->db->where('p.id_planta', $id_planta);
        $query = $this->db->get();
        $datos_geo = $query->row();
        $this->id_geo = $datos_geo->id_geo;
        $this->lat = $datos_geo->lat;
        $this->lng = $datos_geo->lng;
        $this->geocoder = $datos_geo->geocoder;
        $this->elevation = $datos_geo->elevation;
    }

    public function load_geodata_by_operation($id_grafo = null)
    {

        $this->db->select('g.id_geo, g.lat, g.lng, g.geocoder, g.elevation');
        $this->db->from('geodata as g');
        $this->db->join('grafos as o', 'o.id_geo = g.id_geo');
        $this->db->where('o.id_grafo', $id_grafo);
        $query = $this->db->get();
        $datos_geo = $query->row();
        $this->id_geo = $datos_geo->id_geo;
        $this->lat = $datos_geo->lat;
        $this->lng = $datos_geo->lng;
        $this->geocoder = $datos_geo->geocoder;
        $this->elevation = $datos_geo->elevation;
    }

    public function update_geodata($id_geo, $lat, $lng, $elevation, $geocoder)
    {

        $data = array(
            'lat' => $lat,
            'lng' => $lng,
            'geocoder' => $geocoder,
            'elevation' => $elevation,
        );
        $this->db->where('id_geo', $id_geo);
        $this->db->update('geodata', $data);
    }

    public function insertar_geodata($lat, $lng, $elevation, $geocoder)
    {

        $data = array(
            'lat' => $lat,
            'lng' => $lng,
            'geocoder' => $geocoder,
            'elevation' => $elevation,
        );
        $this->db->insert('geodata', $data);
        $id_geo = $this->db->insert_id();
        return $id_geo;
    }

    public function load_operation_attributes($operation_id)
    {

        $this->db->select('t.name, a.value');
        $this->db->from('grafos_attributes as a');
        $this->db->join('grafos_attributes_type as t', 't.id_grafo_attribute_type = a.id_grafo_attribute_type');
        $this->db->where('a.id_grafo', $operation_id);
        $this->db->order_by('t.name', 'ASC');
        $query_attrg = $this->db->get();

        $attributes_list = [];
        foreach ($query_attrg->result_array() as $attr) {

            $attr_node = array('nombre_attr' => $attr['name'], 'valor_attr' => $attr['value']);
            array_push($attributes_list, $attr_node);
        }

        return $attributes_list;
    }

    public function load_plant_attributes($plant_id)
    {

        $this->db->select('T.name, A.value');
        $this->db->from('plantas_attributes as A');
        $this->db->join('plantas_attributes_type as T', 'T.id_planta_attribute_type = A.id_planta_attribute_type');
        $this->db->where('A.id_planta', $plant_id);
        $this->db->order_by('T.name', 'ASC');
        $query_attr = $this->db->get();

        $attributes_list = [];
        foreach ($query_attr->result_array() as $attr) {

            $attr_node = array('nombre_attr' => $attr['name'], 'valor_attr' => $attr['value']);
            array_push($attributes_list, $attr_node);
        }

        return $attributes_list;
    }

    public function load_subsystems($area_id)
    {

        $this->db->select("id_subsystem oid, parent_subsystem parent_oid, s.name text, nickname, '" . NODE_TYPE::SUBSYSTEM . "' node_type, isleaf, st.name ss_type", false);
        $this->db->from('subsystems s');
        $this->db->join('subsystems_type st', 's.id_subsystem_type = st.id_subsystem_type', LEFT_JOIN);
        $this->db->where('id_chain', $area_id);
        $this->db->order_by('id_subsystem', 'asc');
        $query_tree = $this->db->get();

        return $query_tree->result_array();
    }

    public function load_plants($user_id, $privileges, $load_attributes = false)
    {

        $this->db->select("ge.id_geo, ge.lat, ge.lng, ge.geocoder, p.id_planta oid, p.id_chain, p.marker_level, '" . NODE_TYPE::AREA . "' node_type, "
            . "p.id_grafo parent_oid, p.nombre_planta, p.nickname, p.nombre_planta text, pa.value marker, p.id_tipo_planta, NULL as subsystem_ref", false);
        $this->db->from('plantas as p ');
        $this->db->join('geodata as ge', 'p.id_geo = ge.id_geo', LEFT_JOIN);
        $this->db->join('plantas_attributes as pa', 'p.id_planta = pa.id_planta AND pa.id_planta_attribute_type = 9', LEFT_JOIN);

        if ($privileges == 1) {
            $this->db->join('usuarios_perfiles_plantas as u', 'u.id_planta = p.id_planta ');
            $this->db->where('u.id_usuario', $user_id);
        }
        $this->db->order_by('p.id_planta', 'asc');
        $query = $this->db->get();

        $plants = $query->result_array();

        if ($load_attributes) {
            foreach ($plants as $key => $plant) {
                $plants[$key]['attributes'] = $this->load_plant_attributes($plant['oid']);
            }
        }

        return $plants;
    }

    public function load_operations($user_id, $privileges, $load_attributes = false)
    {

        $this->db->select("ge.id_geo, ge.lat, ge.lng, ge.geocoder, g.marker_level, '" . NODE_TYPE::GRAPH . "' node_type, "
            . "g.id_grafo oid, g.id_root, g.nombre_grafo text, g.id_padre parent_oid, NULL as subsystem_ref", false);
        $this->db->from('grafos as g');
        $this->db->join('geodata as ge', 'g.id_geo = ge.id_geo', 'left');

        if ($privileges == 1) {
            $this->db->join('usuarios_perfiles_nodos as n', 'n.id_grafo = g.id_grafo ');
            $this->db->where('n.id_usuario', $user_id);
        }
        $this->db->order_by('g.id_grafo', 'asc');
        $operations_query = $this->db->get();

        $operations = $operations_query->result_array();

        if ($load_attributes) {
            foreach ($operations as $key => $operation) {
                $operations[$key]['attributes'] = $this->load_operation_attributes($operation['oid']);
            }
        }

        return $operations;
    }

    // TODO: definir valores por defectos
    public function empty_node()
    {

        $op['attributes'] = array();
        $op['subsystem_ref'] = null;
        $op['id_grafo'] = 9999999999;
        $op['id_padre'] = "-1";
        $op['geocoder'] = null;
        $op['id_geo'] = null;
        $op['lat'] = null;
        $op['lng'] = null;
        $op['id_root'] = 1;
        $op['node_type'] = NODE_TYPE::GRAPH;
        $op['nombre_grafo'] = 'Sin Asignar';
        $op['plants'] = array();
        $data = [];
        $data['subsystem_ref'] = null;
        $data['node_type'] = NODE_TYPE::GRAPH;
        $op['data'] = $data;

        return $op;
    }

}
