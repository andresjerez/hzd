<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fmeca_rcm_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function get_parts($subsystem_id)
    {
        $this->db->select('co as id, co_subsistem as subsystem_id, nb as name, co_tipo as type_id, vers as version, fe_act as date_modified, std as status')
            ->from('tsgen_subsistems_parts')
            ->where('co_subsistem', $subsystem_id)
            ->where('std', 1);

        $query = $this->db->get();

        if ($query) {
            return $query->result();
        }
        return [];
    }

    public function get_part_fmeca_rcm($part_id)
    {
        $this->db->select('co as id, co_part as part_id, fmeca_std as fmeca_status, rcm_std as rcm_status, clas as class, func as function, falla_funl as functional_failure, modo_falla as failure_mode, elem_mant as maintenable_element, causa as cause, efc_inmto as effect_inmediate, efc_actv as effect_asset, efc_proc as effect_process, sint as symptom, metd_ctrl as control_method, unid_frec as frequency_unit, metd_detc as detection_method, riesgo as risk, evidt as evidente, frec as frequency , consec_seg as consequence_security, consec_ambt as consequence_environment, consec_comunid as consequence_community, consec_rept as consequence_reputation, consec_legal as consequence_legal, consec_finc as consequence_finance, strag as strategy, ctrl_frec as control_frequency, plan_actvd as activity_plan, ds as description, spcd as specialty, plan_frec as plan_frequency, unid_plan_frec as plan_frequency_unit, plan_hh as hh_plan, persnl as staff, durc as duration, parts as parts, herm as tools, sumtr as supplies, sopt_actv as support_assets, std as status')
            ->from('tshzd_subsistems_parts_fmeca_rcm')
            ->where('std', 1)
            ->where('co_part', $part_id);

        $query = $this->db->get();

        if ($query) {
            $result = $query->result();
            $corrected_result = [];

            foreach ($result as $key => $row) {
                // Reemplazamos keys_con_guiones por keys-con-guiones para el frontend
                $corrected_row = []; //
                foreach ($row as $key => $value) {
                    $corrected_row[str_replace('_', '-', $key)] = $value;
                }
                $corrected_result[] = $corrected_row;
            }
            return $corrected_result;
        }

        return [];
    }

    public function update_parts($node)
    {
        // Se separa la Parte de sus FMECA-RCM y se almacenan
        foreach ($node->parts as $part) {

            $fmeca_rcm = $part->fmeca;
            $part->date_modified = $part->date_modified ? $part->date_modified : date("Y-m-d H:i:s");
            unset($part->fmeca);

            $dic_part = array('id' => 'co', 'subsystem_id' => 'co_subsistem', 'name' => 'nb', 'type_id' => 'co_tipo',
                        'version' => 'vers', 'date_modified' => 'fe_act', 'status' => 'std');
            $part_row = [];
            $part_row_insert = [];
            $part_row_update = [];

            foreach ($part as $key => $value) {

                foreach ($dic_part as $clave => $valor) {

                    if ($key == $clave) {

                        $part_row[$valor] = $value;
                    }
                }
            }

            if ($part_row['co'] == NULL) {
                
                $part_row_insert = $part_row;
                unset($part_row_insert['co']);
                $this->db->insert('tsgen_subsistems_parts', $part_row_insert);
                
            }

            else {
                $part_row_update = $part_row;
                $index = $part_row_update['co'];
                unset($part_row_update['co']);

                $this->db->set($part_row_update, 0);
                $this->db->where('co', $index);
                $this->db->update('tsgen_subsistems_parts');
            }

            //$this->db->replace('tsgen_subsistems_parts', $part_row);

            $part_id = $part->id ? $part->id : $this->db->insert_id();

            // Ahora se guarda fmeca y rcm
            $this->update_fmeca_rcm($part_id, $fmeca_rcm);
        }
    }

    public function update_fmeca_rcm($part_id, $fmeca_rcm)
    {
        foreach ($fmeca_rcm->data as $row) {

            // Reemplazamos keys-con-guiones por keys_con_guiones para la db
            $corrected_row = [];
            $corrected_row['part_id'] = $part_id;
            foreach ($row as $key => $value) {
                $corrected_row[str_replace('-', '_', $key)] = $value;
            }

            $dic_fmeca_rcm = array('id' => 'co', 'part_id' => 'co_part', 'fmeca_status' => 'fmeca_std', 'rcm_status' => 'rcm_std', 'class' => 'clas', 'function' => 'func',
                                'functional_failure' => 'falla_funl', 'failure_mode' => 'modo_falla', 'maintenable_element' => 'elem_mant', 'cause' => 'causa', 
                                'effect_inmediate' => 'efc_inmto', 'effect_asset' => 'efc_actv', 'effect_process' => 'efc_proc', 'symptom' => 'sint', 'control_method' => 'metd_ctrl',
                                'frequency_unit' => 'unid_frec' , 'detection_method' => 'metd_detc', 'risk' => 'riesgo', 'evidente' => 'evidt', 'frequency' => 'frec', 'consequence_security' => 'consec_seg',
                                'consequence_environment' => 'consec_ambt', 'consequence_community' => 'consec_comunid', 'consequence_reputation' => 'consec_rept', 'consequence_legal' => 'consec_legal', 
                                'consequence_finance' => 'consec_finc', 'strategy' => 'strag', 'control_frequency' => 'ctrl_frec', 'activity_plan' => 'plan_actvd', 'description' => 'ds', 'specialty' => 'spcd',
                                'plan_frequency' => 'plan_frec', 'plan_frequency_unit' => 'unid_plan_frec', 'hh_plan' => 'plan_hh', 'staff' => 'persnl', 'duration' => 'durc', 'parts' => 'parts', 'tools' => 'herm', 'supplies' => 'sumtr', 'support_assets' => 'sopt_actv', 'status' => 'std');
            $fmeca_rcm_row = [];
            $fmeca_rcm_row_insert = [];
            $fmeca_rcm_row_update = [];

            foreach ($corrected_row as $key => $value) {

                foreach ($dic_fmeca_rcm as $clave => $valor) {

                    if ($key == $clave) {

                        $fmeca_rcm_row[$valor] = $value;
                    }
                }
            }

            if ($fmeca_rcm_row['co'] == NULL) {
                
                $fmeca_rcm_row_insert = $fmeca_rcm_row;
                unset($fmeca_rcm_row_insert['co']);

                $this->db->insert('tshzd_subsistems_parts_fmeca_rcm', $fmeca_rcm_row_insert);
                
            }

            else {
                $fmeca_rcm_row_update = $fmeca_rcm_row;
                $index = $fmeca_rcm_row_update['co'];
                unset($fmeca_rcm_row_update['co']);

                $this->db->set($fmeca_rcm_row_update, 0);
                $this->db->where('co', $index);
                $this->db->update('tshzd_subsistems_parts_fmeca_rcm');
            }

            //$this->db->replace('tshzd_subsistems_parts_fmeca_rcm', $fmeca_rcm_row);
        }
    }

}