<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_account($username, $password)
    {
        $this->db->select('id, username, name, last_name')
            ->from('tsgen_users')
            ->where('username', $username)
            ->where('password', md5($password))
            ->where('status', 1);

        $query = $this->db->get();

        if ($query) {
            return $query->row();
        }

        return null;
    }

    // TODO: crear salt en config
    public function set_token($user)
    {
        $salt = '2d04dc20036db';
        $user->token = md5(base64_encode($user->id) . $salt);

        $this->session->unset_userdata('account');
        $this->session->set_userdata('account', $user);

        return $user->token;
    }

    public function validate_token($token)
    {
        if (isset($this->session->account)) {
            return $this->session->account->token === $token;
        }

        return false;
    }

    public function get_account_by_token($token)
    {

        if ($token && $this->session->has_userdata('account')
            && $this->session->account->token === $token) {
            return $this->session->account;
        }

        return null;
    }

}