<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Risk_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function get_dimensions()
    {
        $this->db->select('co as id, key, nb as name, ds as description, std as status')
            ->from('tsrkm_tipo_dim')
            ->where('std', 1);

        $query = $this->db->get();

        if ($query) {
            return $query->result();
        }
        return [];
    }

    public function get_sub_dimensions($dimension_id = 0)
    {
        $this->db->select('co as id, co_dim as dimension_id, key, nb as name, visb as visible, std as status')
            ->from('tsrkm_tipo_subdim')
            ->where('std', 1)
            ->where('co_dim', $dimension_id);

        $query = $this->db->get();

        if ($query) {
            return $query->result();
        }
        return [];
    }

    public function get_matrix_data($sub_dimension_id = 0)
    {
        $this->db->select('co as id, co_subdim as sub_dimension_id, nvl as level, fact as factor, ds as description, clas as class, std as status')
            ->from('tsrkm_matrix_data')
            ->where('std', 1)
            ->where('co_subdim', $sub_dimension_id);

        $query = $this->db->get();

        if ($query) {
            return $query->result();
        }
        return [];
    }

    /*
     * Se obtiene el uso de cada nivel para evitar borrar niveles y causar inconsistencias
     */
    public function check_data_uses($dimension_id, $matrix_data)
    {

        foreach ($matrix_data as $key => $row) {

            $this->db->select('count(*) as use_count')
                ->from('tshzd_subsistems_parts_fmeca_rcm')
                ->where('std', 1);

            switch ($dimension_id) {
                case 1: // Frequency
                    $this->db->where('frec', $row->level);
                    break;
                case 2: // Consequence
                    $this->db->where($row->level . ' IN (consec_seg, consec_ambt, consec_comunid, consec_rept, consec_legal, consec_finc)', null, false);
                    break;
                case 3: // Detection
                    $this->db->where('metd_detc', $row->level);
                    break;
            };

            $query = $this->db->get();

            $use_count = 0;
            if ($query) {
                $use_count = $query->row()->use_count;
            }
            $matrix_data[$key]->use_count = $use_count;
        }
        
        return $matrix_data;
    }

    public function update_matrix_data($sub_dimension)
    {
        foreach ($sub_dimension->data as $matrix_data) {
            $md = $matrix_data;
            $md->sub_dimension_id = $sub_dimension->id;
            unset($md->use_count); // Campo solo usado para validacion frontend
            
            $dic_md = array('id' => 'co', 'sub_dimension_id' => 'co_subdim', 'level' => 'nvl', 'factor' => 'fact', 'description' => 'ds', 'class' => 'clas', 'status' => 'std');
            $md_row = [];
            $md_row_insert = [];
            $md_row_update = [];

            foreach ($md as $key => $value) {

                foreach ($dic_md as $clave => $valor) {

                    if ($key == $clave) {

                        $md_row[$valor] = $value;
                    }
                }
            }

            //$this->db->replace('tsrkm_matrix_data', $md_row);
              
            if ($md_row['co'] == NULL) {
                
                $md_row_insert = $md_row;
                unset($md_row_insert['co']);

                $this->db->insert('tsrkm_matrix_data', $md_row_insert);
                
            }

            else {
                $md_row_update = $md_row;
                $index = $md_row_update['co'];
                unset($md_row_update['co']);

                $this->db->set($md_row_update, 0);
                $this->db->where('co', $index);
                $this->db->update('tsrkm_matrix_data');
            }

        }
    }

    public function update_visibility($datos)
    {
        $update = array('visb' => $datos[1]);
        $this->db->set($update, 0);
        $this->db->where("key", $datos[0]);
        $this->db->update('tsrkm_tipo_subdim');
    }

}