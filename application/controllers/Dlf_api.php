<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Dlf_api extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        //        header('Access-Control-Allow-Origin: http://suitedev.rmessuite.com');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }

        // Validate user account against this actions
        /*
    TODO: descomentar en ambiente de produccion!

    $secure_actions = ['save_changes', 'save_fmeca_rcm_changes', 'save_decision_tree_structure'];
    if (in_array($this->router->fetch_method(), $secure_actions)
    && !$this->session->has_userdata('account')) {
    $data['status'] = false;
    $data['message'] = 'unauthorized';
    echo json_encode($data);
    die();
    }*/
    }

    public function get_tree()
    {
        $tree = $this->load_geodata_markers();
        echo json_encode($tree);
    }

    public function load_geodata_markers($user_id = 1, $privileges = 0)
    {
        // Se cargan nodos (operaciones) y plantas (areas) para el usuario indicado
        $this->load->model('Geodata_model');
        $raw_operations = $this->Geodata_model->load_operations($user_id, $privileges, true);
        $operations = $this->get_operation_nodes($raw_operations);

        $raw_areas = $this->Geodata_model->load_plants($user_id, $privileges, true);
        $areas = $this->get_area_nodes($raw_areas);

        $structure = array();
        $empty_node = $this->Geodata_model->empty_node();
        $operation_filtered_children = array();
        foreach ($operations as $operation) {

            foreach ($areas as $key => $area) {

                if ($area['parent_oid'] === $operation['oid']) {
                    
                    // Se obtienen los subsystems del area
                    $raw_subsystems = $this->Geodata_model->load_subsystems($area['data']['id_chain']);
                    $subsystems = $this->get_subsystem_nodes($raw_subsystems);

                    $area_children = $this->buildTree($subsystems);

                    // Se enlazan los subsystems al area padre o a sus excepciones
                    $area_filtered_children = array();
                    foreach ($area_children as $children) {

                        switch ($children['parent_oid']) {
                            case 0:
                                // Este hijo, representa RMES asociado a su area por lo que sus hijos pasan a ser parte del area
                                $area['data']['subsystem_ref'] = $children['oid'];
                                $area_filtered_children = array_merge($area_filtered_children, $children['children']);
                                break;

                            case -1:
                                // Este hijo, representa RMES asociado a su operacion por lo que sus hijos pasan a ser parte de la operacion
                                $operation['data']['subsystem_ref'] = $children['oid'];
                                $operation_filtered_children[] = array_merge($operation_filtered_children, $children['children']);
                                break;

                            default:
                                // Por defecto, se agrega el hijo (y con ello sus hijos) al area correspondiente
                                $area_filtered_children[] = $children;
                        }
                    }
                    $area['children'] = $area_filtered_children;

                    // Se asigna el area y se remueve de las areas por iterar
                    $operation['children'][] = $area;

                    // En contexto foreach de php, es seguro remover elementos
                    unset($areas[$key]);
                }
            }

            $structure[] = $operation;
        }

        // Si hay areas sin padre, se agregan al nodo vacio
        foreach ($areas as $area) {

            $area['children'] = $this->Geodata_model->load_subsystems($area['id_chain']);
            $empty_node['children'][] = $area;
        }

        if (!empty($empty_node['children'])) {
            $structure[] = $empty_node;
        }

        return $this->buildTree($structure);
    }

    private function buildTree($dataset)
    {
        $tree = array();

        /* Most datasets in the wild are enumerative arrays and we need associative array
        where the same ID used for addressing parents is used. We make associative
        array on the fly */
        $references = array();
        foreach ($dataset as $id => &$node) {
            // Add the node to our associative array using it's ID as key
            $references[$node['oid']] = &$node;

            // Se crea el espacio para los hijos
            if (!isset($node['children'])) {
                $node['children'] = array();
            }

            // Add empty placeholder for children
            // It it's a root node, we add it directly to the tree
            if ($node['parent_oid'] == 0 || $node['parent_oid'] == -1) {
                $tree[$node['oid']] = &$node;
            } else {
                // It was not a root node, add this node as a reference in the parent.
                $references[$node['parent_oid']]['children'][] = &$node;
            }
        }

        return array_values($tree);
    }

    /**
     * Se formatean los datos de operacion y se entregan en un objeto estandar nodo
     * @param Array $operations
     * @return Array $nodes
     */
    private function get_operation_nodes($operations)
    {

        $nodes = array();
        foreach ($operations as $operation) {
            $geodata = array(
                'id_geo' => $operation['id_geo'],
                'geocoder' => $operation['geocoder'],
                'lat' => $operation['lat'],
                'lng' => $operation['lng'],
            );

            $data = array(
                'subsystem_ref' => $operation['subsystem_ref'],
                'id_root' => $operation['id_root'],
                'marker_level' => $operation['marker_level'],
                'marker' => null,
                'geodata' => $geodata,
                'attributes' => $operation['attributes'],
            );

            $icon = base_url('assets/img/jstree_icons/nodeicon.png');

            $node = array(
                'oid' => $operation['oid'],
                'parent_oid' => $operation['parent_oid'],
                'node_type' => $operation['node_type'],
                'type' => 'graph',
                'text' => $operation['text'],
                //'icon' => $icon,
                'data' => $data,
            );

            $nodes[] = $node;
        }

        return $nodes;
    }

    /**
     * Se formatean los datos de area y se entregan en un objeto estandar nodo
     * @param Array $areas
     * @return Array $nodes
     */
    private function get_area_nodes($areas)
    {
        $nodes = array();
        foreach ($areas as $area) {
            $geodata = array(
                'id_geo' => $area['id_geo'],
                'geocoder' => $area['geocoder'],
                'lat' => $area['lat'],
                'lng' => $area['lng'],
            );

            $data = array(
                'subsystem_ref' => $area['subsystem_ref'],
                'marker_level' => $area['marker_level'],
                'id_chain' => $area['id_chain'],
                'nombre_planta' => $area['nombre_planta'],
                'id_tipo_planta' => $area['id_tipo_planta'],
                'marker' => $area['marker'],
                'geodata' => $geodata,
                'attributes' => $area['attributes'],
            );

            $icon = base_url('assets/img/jstree_icons/nodeicon.png');
            $node = array(
                'oid' => $area['oid'],
                'parent_oid' => $area['parent_oid'],
                'node_type' => $area['node_type'],
                'type' => 'area',
                'text' => $area['text'],
                //'icon' => $icon,
                'nickname' => $area['nickname'],
                'data' => $data,
            );

            $nodes[] = $node;
        }

        return $nodes;
    }

    /**
     * Se formatean los datos de subsystem y se entregan en un objeto estandar nodo
     * @param Array $subsystems
     * @return Array $nodes
     */
    private function get_subsystem_nodes($subsystems)
    {
        $nodes = array();

        foreach ($subsystems as $subsystem) {

//        if ($subsystem['isleaf'] == 1) {
            //        $type = 'equipment';
            //        } else {
            //        $type = $subsystem['ss_type'];
            //        }
            //
            //        //$children['icon'] = $icon;

            $data = array(
                'subsystem_ref' => $subsystem['oid'],
                'marker_level' => 0,
                'isleaf' => (string) $subsystem['isleaf'],
            );

            $node = array(
                'oid' => $subsystem['oid'],
                'parent_oid' => $subsystem['parent_oid'],
                'node_type' => $subsystem['node_type'],
                'type' => 'equipment', //$subsystem['ss_type'], modificado para centinela
                'text' => $subsystem['text'],
                //'icon' => $icon,
                'nickname' => $subsystem['nickname'],
                'data' => $data,
            );

            $nodes[] = $node;
        }

        return $nodes;
    }

    public function get_tree_to_csv($node, $csv_array, $route)
    {
        /* if (sizeof($route) > 5) {
        return $csv_array;
        } */

        if (sizeof($node['children']) > 0) {
            $route[] = $node['text'];
            for ($i = 0; $i < sizeof($node['children']); $i++) {
                $csv_array = $this->get_tree_to_csv($node['children'][$i], $csv_array, $route);
            }
            array_pop($route);
        } else {
            $route[] = $node['text'];
            $route[] = $node['oid'];
            $csv_array[] = $route;
            array_pop($route);
        }

        return $csv_array;
    }

}
