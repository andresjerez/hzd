<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends CI_Controller
{
    /**
     * Api constructor.
     * Declara el control de CORS para toda la API
     * Adicionalmente termina las consultas preflight para que no ejecute codigo innecesariamente.
     * @see https://developer.mozilla.org/en-US/docs/Glossary/Preflight_request
     */
    public function __construct()
    {
        parent::__construct();

//        header('Access-Control-Allow-Origin: http://suitedev.rmessuite.com');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }

    

    }

    public function index()
    {
        echo 'hola!';
    }

    public function do_login()
    {
        $data['status'] = false;
        $userdata = json_decode(file_get_contents('php://input'))->userdata;

        if ($userdata) {
            $username = $userdata->username;
            $password = $userdata->password;

            $this->load->model('Account_model');

            $user = $this->Account_model->get_account($username, $password, true);

            if ($user) {
                $this->Account_model->set_token($user);
                $data['user'] = $user;
                $data['status'] = true;
            }
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function recover_session()
    {

        $data['status'] = false;
        $token = json_decode(file_get_contents('php://input'))->token;

        $this->load->model('Account_model');
        $user = $this->Account_model->get_account_by_token($token);

        if ($user) {
            $data['user'] = $user;
            $data['status'] = true;
        }

        echo json_encode($data);
    }

    public function destroy_session()
    {
        $this->session->sess_destroy();
        $data['status'] = true;
        echo json_encode($data);
    }

    public function risk_matrix_dimensions($return = false)
    {
        $data['status'] = true;

        $this->load->model('Risk_model');
        $dimensions = $this->Risk_model->get_dimensions();

        foreach ($dimensions as $i => $dimension) {
            $sub_dimensions = $this->Risk_model->get_sub_dimensions($dimension->id);

            foreach ($sub_dimensions as $j => $sub_dimension) {

                $matrix_data = $this->Risk_model->get_matrix_data($sub_dimension->id);
                $matrix_data = $this->Risk_model->check_data_uses($dimension->id, $matrix_data);
                $sub_dimensions[$j]->data = $matrix_data;

            }

            $dimensions[$i]->matrix = $sub_dimensions;
        }

        $data['dimensions'] = $dimensions;

        if ($return) { // Return so it can be used by other functions
            return $data['dimensions'];
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data, JSON_NUMERIC_CHECK));
        }
    }

    public function save_changes()
    {

        $risk_dimensions = json_decode(file_get_contents('php://input'))->risk_dimensions;

        $data['status'] = true;

        $this->load->model('Risk_model');

        foreach ($risk_dimensions as $dimension) {
            foreach ($dimension->matrix as $sub_dimension) {

                $this->Risk_model->update_matrix_data($sub_dimension);

            }
        }

        $data['dimensions'] = $this->risk_matrix_dimensions(true);

        echo json_encode($data);

    }
    
    public function change_visibility()
    {

        $datos = json_decode(file_get_contents('php://input'))->datos;

        $data['status'] = true;

        $this->load->model('Risk_model');

        $this->Risk_model->update_visibility($datos);

        echo json_encode($data);
    }

    public function get_RiskMatrix_Used()
    {

        $this->load->model('Hazop_hazid_model');
        $isUsed = $this->Hazop_hazid_model->get_riskMatrix_used();

        $data['status'] = true;
        $data = $isUsed;

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function fmeca_risk_matrix_options()
    {

        $data['status'] = true;

        $this->load->model('Risk_model');
        $dimensions = $this->Risk_model->get_dimensions();

        $risk_matrix_options = [];
        foreach ($dimensions as $i => $dimension) {
            $sub_dimensions = $this->Risk_model->get_sub_dimensions($dimension->id);

            foreach ($sub_dimensions as $j => $sub_dimension) {
                $key = 'risk-matrix_' . $dimension->key . '-' . $sub_dimension->key;
                $risk_matrix_options[$key] = array(
                    'key' => $sub_dimension->key,
                    'name' => $sub_dimension->name,
                    'data' => $this->Risk_model->get_matrix_data($sub_dimension->id),
                );
            };
        }

        $data['risk_matrix_options'] = $risk_matrix_options;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function parts()
    {
        $subsystem_id = $this->input->get('subsystem_id');
        $data['status'] = false;

        if ($subsystem_id) {

            $this->load->model('Fmeca_rcm_model');
            $this->load->model('Hazop_hazid_model');
            $parts = $this->Fmeca_rcm_model->get_parts($subsystem_id);

            foreach ($parts as $key => $value) {
                $parts[$key]->fmeca = array(
                    'fmeca_status' => false,
                    'rcm_status' => false,
                    'data' => $this->Fmeca_rcm_model->get_part_fmeca_rcm($value->id), // key fmeca contiene datos de fmeca y rcm. TODO: refactorizar nombre a fmeca_rcm
                );
                $parts[$key]->hazop = array(
                    'hazop_status' => false,
                    'data' => $this->Hazop_hazid_model->get_part_hazop($value->id),
                );
                $parts[$key]->hazid = array(
                    'hazid_status' => false,
                    'data' => $this->Hazop_hazid_model->get_part_hazid($value->id),
                );
            }

            $data['parts'] = $parts;
            $data['status'] = true;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function parts_multiple()
    {
        $subsystems_ids = json_decode(file_get_contents('php://input'))->subsystems_ids;
        $data['status'] = true;

        if (!$subsystems_ids || empty($subsystems_ids)) {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode([]));
            return;
        }

        $this->load->model('Fmeca_rcm_model');
        $data['subsystems'] = [];
        foreach ($subsystems_ids as $subsystem_id) {

            if ($subsystem_id) {

                $parts = $this->Fmeca_rcm_model->get_parts($subsystem_id);

                foreach ($parts as $key => $value) {
                    $parts[$key]->fmeca = array(
                        'fmeca_status' => false,
                        'rcm_status' => false,
                        'data' => $this->Fmeca_rcm_model->get_part_fmeca_rcm($value->id), // key fmeca contiene datos de fmeca y rcm. TODO: refactorizar nombre a fmeca_rcm
                    );
                }

                $data['subsystems']['s-' . $subsystem_id] = array(
                    'parts' => $parts,
                );
            }

        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function save_hazop_changes()
    {

        $hazop_matrix = json_decode(file_get_contents('php://input'))->hazop_matrix;
        $data['status'] = true;

        $this->load->model('Hazop_hazid_model');
        foreach ($hazop_matrix as $node) {
            $this->Hazop_hazid_model->update_parts_hazop($node);
        }

        echo json_encode($data);
    }

    public function save_hazop_delete()
    {

        $hazop_matrix = json_decode(file_get_contents('php://input'))->hazop_matrix;
        $data['status'] = true;

        $this->load->model('Hazop_hazid_model');
        foreach ($hazop_matrix as $node) {
            $this->Hazop_hazid_model->delete_parts($node);
        }

        echo json_encode($data);
    }

    public function hazop_guide_words()
    {

        $this->load->model('Hazop_hazid_model');
        $guide_words = $this->Hazop_hazid_model->get_guide_words();

        $data['status'] = true;
        $data['guide_words'] = $guide_words;

        echo json_encode($data);
    }

    public function save_hazop_guide_words()
    {

        $guide_words = json_decode(file_get_contents('php://input'))->guide_words;
        $data['status'] = true;

        $this->load->model('Hazop_hazid_model');
        $this->Hazop_hazid_model->update_guide_words($guide_words);

        echo json_encode($data);
    }

    public function hazop_part_types()
    {
        $data['status'] = true;

        $this->load->model('Hazop_hazid_model');
        $part_types = $this->Hazop_hazid_model->get_part_types();

        $part_types_full = [];
        if ($part_types && !empty($part_types)) {
            foreach ($part_types as $part_type) {
                $part_type->parameters = $this->Hazop_hazid_model->get_part_type_parameters($part_type->id);
                $part_type->actions = $this->Hazop_hazid_model->get_part_type_actions($part_type->id);

                $part_types_full[] = $part_type;
            }
        }

        $data['part_types'] = $part_types_full;

        echo json_encode($data);
    }

    public function save_hazop_part_types()
    {
        $part_types = json_decode(file_get_contents('php://input'))->part_types;
        $data['status'] = true;

        $this->load->model('Hazop_hazid_model');
        $this->Hazop_hazid_model->update_part_types($part_types);

        echo json_encode($data);
    }


    public function save_hazid_changes()
    {

        $hazid_matrix = json_decode(file_get_contents('php://input'))->hazid_matrix;
        $data['status'] = true;

        $this->load->model('Hazop_hazid_model');

        foreach ($hazid_matrix as $node) {
            
            $this->Hazop_hazid_model->update_parts_hazid($node); 
        }

        echo json_encode($data);
    }

    public function save_hazid_delete()
    {

        $hazid_matrix = json_decode(file_get_contents('php://input'))->hazid_matrix;
        $data['status'] = true;

        $this->load->model('Hazop_hazid_model');

        foreach ($hazid_matrix as $node) {
            
            $this->Hazop_hazid_model->delete_parts($node); 
        }

        echo json_encode($data);
    }
    
    public function hazid_categories()
    {
        $data['status'] = true;

        $this->load->model('Hazop_hazid_model');
        $categories = $this->Hazop_hazid_model->get_hazid_categories();

        $categories_full = [];
        if ($categories && !empty($categories)) {
            foreach ($categories as $category) {
                $category->guide_words = $this->Hazop_hazid_model->get_category_guide_words($category->id);
                $categories_full[] = $category;
            }
        }

        $data['hazid_categories'] = $categories_full;

        echo json_encode($data);
    }

    public function save_hazid_categories()
    {
        $hazid_categories = json_decode(file_get_contents('php://input'))->hazid_categories;
        $data['status'] = true;

        $this->load->model('Hazop_hazid_model');
        $this->Hazop_hazid_model->update_hazid_categories($hazid_categories);

        echo json_encode($data);
    }

    public function decision_tree_structure()
    {
        $this->load->helper('file');
        $decision_tree_json = read_file(FCPATH . '/assets/decision_tree.json');

        $data['status'] = true;
        $data['structure'] = json_decode($decision_tree_json, JSON_NUMERIC_CHECK);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function save_decision_tree_structure()
    {
        $structure = json_decode(file_get_contents('php://input'))->structure;

        $this->load->helper('file');
        write_file(FCPATH . '/assets/decision_tree.json', json_encode($structure));

        $data['status'] = true;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function save_fmeca_rcm_changes()
    {

        $fmeca_rcm_matrix = json_decode(file_get_contents('php://input'))->fmeca_rcm_matrix;
        $data['status'] = true;

        $this->load->model('Fmeca_rcm_model');
        foreach ($fmeca_rcm_matrix as $node) {
            $this->Fmeca_rcm_model->update_parts($node);
        }

        echo json_encode($data);
    }
}