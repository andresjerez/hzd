<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
 */
defined('SHOW_DEBUG_BACKTRACE') or define('SHOW_DEBUG_BACKTRACE', true);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
 */
defined('FILE_READ_MODE') or define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') or define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') or define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') or define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
 */
defined('FOPEN_READ') or define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') or define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') or define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') or define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') or define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') or define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') or define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') or define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
 */
defined('EXIT_SUCCESS') or define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') or define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') or define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') or define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') or define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') or define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') or define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') or define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') or define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') or define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('ADDR_CSS', 'assets/css/');
define('ADDR_IMG', 'assets/img/');
define('ADDR_JS', 'assets/js/');

// Definicion de tipos de areas (plantas). Deben concordar con la tabla 'plants_type'
define('AREA_TYPE_REGULAR', 1);
define('AREA_TYPE_OPERATION', 2);
define('AREA_TYPE_CONGLOMERATE', 3);

// Variables de sesión
define('SESSION_DEFAULT_DURATION', 7200);
define('SESSION_LONG_DURATION', 604800);

define('LIGHTWEIGHT_MODE', false);

define('LEFT_JOIN', 'LEFT');
define('RIGHT_JOIN', 'RIGHT');

define('RMES_SERVER_ADDRESS', 'http://190.101.122.216:8080/rest-service/calculate/series');

define('DATE_FORMAT_ISO', 'Y-m-d H:i:s');
define('DATE_FORMAT_ISO_SHORT', 'Y-m-d');
define('DATE_FORMAT_LOCAL', 'd/m/Y H:i:s');
define('DATE_FORMAT_LOCAL_SHORT', 'd/m/Y');

define('GOAL_SUCCESS', 90); // Green
define('GOAL_WARNING', 80); // Yellow
define('GOAL_DANGER', 70); // Red

abstract class NODE_TYPE
{

    const GRAPH = 0;
    const AREA = 1;
    const SUBSYSTEM = 2;

}

abstract class METRICS_TYPE
{

    const DAY = 1;
    const WEEK = 2;
    const MONTH = 3;
    const YTD = 5;
    const MTD = 6;
    const MY = 7;
    const FMTD = 8;

}

abstract class SETTINGS_TYPE
{

    const NODE_SELECTION = 1;
    const GENERAL_INDICATORS = 2;
    const DASHBOARD_INDICATORS = 3;
    const ADVANCED_VIEW = 4;

}

abstract class INDICATORS_TYPE
{

    const AVAILABILITY = 1;
    const UTILIZATION = 2;
    const EFFECTIVE_UTILIZATION = 3;
    const MTBF = 4;
    const MTBS = 5;
    const MTTR = 6;
    const JACK_KNIFE = 7;
    const MIX_MAINTENANCE_HRS = 8;
    const MIX_MAINTENANCE_QTY = 9;
    const CRITICALITY = 10;
    const MTTI = 11;
    const FAILURE_COST = 12;
    const PARETO = 13;
    const FAILURE_MODE = 14;
    const RELIABILITY = 15;
    const DISTRIBUTION = 16;
    const OEE = 17;
    const PRODUCTION = 18;
    const JACK_KNIFE_FAILURE_MODE = 19;
    const NOMINAL_PRODUCTION_FACTOR = 20;
    const CONSOLIDATED_KPIS = 21;
    const EFFECTIVE_RUNTIME = 24;
    const AVAILABILITY_GOALS = 25;
    const UTILIZATION_GOALS = 26;
    const EFFECTIVE_UTILIZATION_GOALS = 27;
    const RCA = 28;
    const PLANNED_WORK = 999;
    const MATRIX_PLAN = 1000;
    const WEEKLY_PROGRAM = 1001;
    const ADHERENCE = 1002;
    const M_AVAILABILITY = 100;
    const M_UTILIZATION = 101;
    const M_EFFECTIVE_UTILIZATION = 102;

    private static $aliases = array(
        self::AVAILABILITY => 'A',
        self::UTILIZATION => 'U',
        self::EFFECTIVE_UTILIZATION => 'UE',
        self::MTBF => 'MTBF',
        self::MTBS => 'MTBS',
        self::MTTR => 'MTTR',
        self::JACK_KNIFE => 'JK',
        self::MIX_MAINTENANCE_HRS => 'MMH',
        self::MIX_MAINTENANCE_QTY => 'MMQ',
        self::CRITICALITY => 'CRIT',
        self::MTTI => 'MTTI',
        self::FAILURE_COST => 'CF',
        self::PARETO => 'P',
        self::FAILURE_MODE => 'FM',
        self::RELIABILITY => 'R',
        self::DISTRIBUTION => 'D',
        self::OEE => 'OEE',
        self::PRODUCTION => 'P',
        self::JACK_KNIFE_FAILURE_MODE => 'FMJK',
        self::NOMINAL_PRODUCTION_FACTOR => 'FNP',
        self::CONSOLIDATED_KPIS => 'CKPIs',
        self::EFFECTIVE_RUNTIME => 'CME',
        self::AVAILABILITY_GOALS => 'AG',
        self::UTILIZATION_GOALS => 'CMG',
        self::EFFECTIVE_UTILIZATION_GOALS => 'UG',
        self::RCA => 'RCA',
        self::PLANNED_WORK => 'PLANNED_WORK',
        self::MATRIX_PLAN => 'MATRIX_PLAN',
        self::WEEKLY_PROGRAM => 'WEEKLY_PROGRAM',
        self::ADHERENCE => 'ADHERENCE',
    );

    public static function get_alias($key)
    {
        return self::$aliases[$key];
    }

}

abstract class MARKERS
{

    const LEVEL_1 = 1;
    const LEVEL_2 = 2;
    const LEVEL_3 = 3;
    const DEFAULT_DRAW_LEVEL = MARKERS::LEVEL_1;
    const DEFAULT_NEUTRAL_COLOR = 'black';
    const DEFAULT_SELECTED_COLOR = 'orange';
    const ICON_DEFAULT_1 = 'factory';
    const ICON_DEFAULT_2 = 'mine';
    const ICON_DEFAULT_3 = 'factory';

}
